module db.user;

import vibe.core.sync;
import vibe.db.mongo.collection;
import vibe.data.bson;
import vibe.http.server;

import core.time;

import std.datetime.systime;
import std.random;

import cachetools;
import mongoschema;

import crypt.password;

import db;
import util.caches;

struct User
{
	@mongoUnique string osuID;
	string username;
	SchemaDate registeredAt = SchemaDate.now;
	ulong sessionToken;
	bool admin, banned;
	string apiToken;

	SchemaDate lastMapCreate;

	bool valid()
	{
		return bsonID != BsonObjectID.init && osuID.length && username.length && !banned;
	}

	void createdMap()
	{
		lastMapCreate = SchemaDate.now;
	}

	mixin MongoSchema;

	void resetSession()
	{
		sessionToken = uniform!ulong;
	}

	DocumentRange!ActiveLobby activeLobbies()
	{
		return ActiveLobby.findByCreator(bsonID);
	}

	DocumentRange!Lobby lobbyHistory()
	{
		return Lobby.findByCreator(bsonID);
	}

	static UserCacheInfo getCachedInfo(BsonObjectID id)
	{
		userCacheMutex.lock();
		scope (exit)
			userCacheMutex.unlock();
		auto existing = userIdNameCache.get(id);
		if (!existing.isNull)
			return existing.get;

		auto user = User.tryFindById(id);
		string name;
		string userid;
		if (!user.isNull)
		{
			name = user.get.username;
			userid = user.get.osuID;
		}

		auto ret = UserCacheInfo(name, userid);
		userIdNameCache.put(id, ret);
		return ret;
	}

	static string getName(BsonObjectID id)
	{
		return getCachedInfo(id).name;
	}

	static string getOsuID(BsonObjectID id)
	{
		return getCachedInfo(id).id;
	}
}

User validateUser(scope HTTPServerRequest req, scope HTTPServerResponse res)
{
	if (!req.session)
		return User.init;

	BsonObjectID userid = req.session.get("user", BsonObjectID.init);
	ulong token = req.session.get!ulong("token");

	if (userid == BsonObjectID.init)
		return User.init;

	auto user = User.tryFindById(userid, User.init);
	if (!user.bsonID.valid || user.sessionToken != token)
	{
		req.session.remove("user");
		req.session.remove("token");
		return User.init;
	}
	return user;
}

User validateAPIUser(scope HTTPServerRequest req, scope HTTPServerResponse res, out string apiKey)
{
	apiKey = req.query.get("apikey", req.form.get("apikey", ""));

	if (!apiKey.length)
		return validateUser(req, res);

	auto u = User.tryFindOne(query!User.apiToken(apiKey), User.init);
	if (!u.apiToken.length)
		return User.init;

	return u;
}

string makeToken(scope HTTPServerRequest req, scope HTTPServerResponse res)
{
	auto session = req.session;
	if (!session)
		session = res.startSession();

	auto existing = session.get!long("ctokendate", 0);
	if (Clock.currStdTime - existing < 30.minutes.total!"hnsecs")
	{
		string ret = session.get("ctoken", "");
		if (ret.length)
			return ret;
	}

	string ret = generateToken;
	session.set("ctoken", ret);
	session.set("ctokendate", Clock.currStdTime);
	return ret;
}

bool validateToken(scope HTTPServerRequest req, ref string token)
{
	if (!req.session)
		return false;

	auto date = req.session.get!long("ctokendate", 0);
	if (Clock.currStdTime - date > 4.hours.total!"hnsecs")
		return false;

	if (req.session.get("ctoken", "") == token)
	{
		if (req.headers.get("X-Refresh-Token", "") != "false")
		{
			req.session.set("ctoken", token = generateToken);
			req.session.set("ctokendate", Clock.currStdTime);
		}
		return true;
	}
	else
		return false;
}

enum authenticateUser(bool required) = "User user = validateUser(req, res);" ~ (
			required ? "if (!user.valid) return res.redirect(WebRoot ~ `/login`);" : "");
enum authenticateAPIUser(bool required) = "string apiKey; User user = validateAPIUser(req, res, apiKey);" ~ (required
			? "if (!user.valid) return res.redirect(WebRoot ~ `/login`);"
			: "if (!user.valid) apiKey = null;");
enum authenticateAdmin = "User user = validateUser(req, res); if (!user.valid || !user.admin) return res.redirect(WebRoot ~ `/login`);";

struct SavedBot
{
	string username, password;
	/// User
	BsonObjectID owner;

	mixin MongoSchema;
}
