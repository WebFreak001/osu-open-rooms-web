module db.game;
@safe:

import vibe.core.core;
import vibe.core.log;
import vibe.core.sync;
import vibe.data.bson;
import vibe.db.mongo.collection;

import mongoschema;
import mongoschema : encodeMember = encode, decodeMember = decode;

import core.time;

import std.algorithm;
import std.array;
import std.conv;
import std.datetime.systime;
import std.datetime.timezone;
import std.math;
import std.meta;
import std.string;
import std.traits;

import bancho.irc;

import cachetools;

import osu.api.map;
import osu.api.user;

import util.autopp;
import util.caches;

import orw.protocol.types;

SysTime currentTime()
{
	return Clock.currTime(UTC());
}

struct Lobby
{
	string name;
	BsonObjectID creator;
	SchemaDate createdAt = SchemaDate.now;
	SerializableDuration ttl;
	string gameId, apiId;
	string settingsMode;
	Bson settings;
	string closeReason;

	@trusted mixin MongoSchema;

	static DocumentRange!Lobby findByCreator(BsonObjectID creator)
	{
		return Lobby.findRange(["creator": creator]);
	}
}

struct ActiveLobby
{
	Lobby lobby;
	BsonObjectID song;
	int pid;

	@trusted mixin MongoSchema;

	static DocumentRange!ActiveLobby findByCreator(BsonObjectID creator)
	{
		return ActiveLobby.findRange(["lobby.creator": creator]);
	}

	static auto countByCreator(BsonObjectID creator)
	{
		return ActiveLobby.count(["lobby.creator": creator]);
	}

	static DocumentRange!ActiveLobby findByMaplist(BsonObjectID maplist)
	{
		return ActiveLobby.findRange(["lobby.settings.maps": maplist]);
	}

	static DocumentRange!ActiveLobby findDead()
	{
		return ActiveLobby.findRange(["pid": 0]);
	}
}

private void putString(ref Appender!string ret, string s)
{
	auto quote = s.indexOfAny(`"'`);
	if (!s.length || s.indexOfAny(" ,=;:+()") != -1 || quote != -1)
	{
		if (quote != -1 && s[quote] == '\'')
		{
			ret.put('"');
			ret.put(s.replace("\\", "\\\\").replace("\"", "\\\""));
			ret.put('"');
		}
		else
		{
			ret.put('\'');
			ret.put(s.replace("\\", "\\\\").replace("\'", "\\\'"));
			ret.put('\'');
		}
	}
	else
		ret.put(s);
}

enum parsable;

struct Playlist
{
	enum adminLimit = 5000;
	enum userLimit = 500;

	struct AutoPPPick
	{
		enum DT : ubyte
		{
			any = ubyte.max,
			no = 0,
			yes = 1,
			ht = 2
		}

		enum TriState : ubyte
		{
			any = ubyte.max,
			no = 0,
			yes = 1
		}

		string dataSource;
		GameMode mode;
		/// Comment generator function.
		/// Replaces {{...}} with variable output.
		/// Variables: dt, ht, hd, hr, fl, beatmap, mapset, overweight, pp, artist,
		/// title, version, difficulty, length, bpm, passcount, lastupdate, genre, language
		/// Booleans can be converted to strings by appending `...|yes|no`
		/// Floats can be truncated to n digits with `|n`
		string comments;

		@parsable
		{
			// keep updated with maplists.js
			int minPP, maxPP = int.max;

			int minLength, maxLength = int.max;
			int minKeys, maxKeys = int.max;
			int minBpm, maxBpm = int.max;
			double minDiff = 0, maxDiff = 100;
			double minOverweight = 0, maxOverweight = 1e5;
			int minPassCount, maxPassCount = int.max;
			int minUpdateHours, maxUpdateHours = int.max;
			MapLanguage langs = MapLanguage.any;
			MapGenre genres = MapGenre.any;
		}

		union
		{
			@schemaIgnore struct
			{
				DT dt;
				TriState hd;
				TriState hr;
				TriState fl;
			}

			uint mods;
		}

		void toRecipe(ref Appender!string dst) const
		{
			dst.put("ppv1 ");
			putString(dst, dataSource);
			if (comments.length)
			{
				dst.put(' ');
				putString(dst, comments);
			}

			putRecipeFilters(dst, this);
		}

		private static void putTriState(ref Appender!string dst, string key, TriState state)
		{
			switch (state)
			{
			case TriState.no:
				dst.put(' ');
				dst.put(key);
				dst.put("=no");
				break;
			case TriState.yes:
				dst.put(' ');
				dst.put(key);
				break;
			case TriState.any:
			default:
				break;
			}
		}

		private static void putDT(ref Appender!string dst, DT state)
		{
			switch (state)
			{
			case DT.ht:
				dst.put(" HT");
				break;
			case DT.no:
				dst.put(" DT=no");
				break;
			case DT.yes:
				dst.put(" DT");
				break;
			case DT.any:
			default:
				break;
			}
		}

		private static void putRecipeFilters(ref Appender!string dst, Playlist.AutoPPPick pp)
		{
			alias Parsable = getSymbolsByUDA!(Playlist.AutoPPPick, parsable);

			static foreach (filterable; Parsable)
			{
				{
					enum defaultValue = __traits(getMember, Playlist.AutoPPPick.init, filterable.stringof);
					auto val = __traits(getMember, pp, filterable.stringof);

					if (abs(val - defaultValue) > 1e-4)
					{
						dst.put(' ');
						dst.put(filterable.stringof);
						dst.put('=');
						dst.put(val.to!string);
					}
				}
			}

			putTriState(dst, "HD", pp.hd);
			putTriState(dst, "HR", pp.hr);
			putTriState(dst, "FL", pp.fl);
			putDT(dst, pp.dt);
		}

		ModNumber requiredMods()
		{
			ModNumber ret;
			if (dt == DT.ht)
				ret |= ModNumber.HalfTime;
			else if (dt == DT.yes)
				ret |= ModNumber.DoubleTime;
			if (hd == TriState.yes)
				ret |= ModNumber.Hidden;
			if (hr == TriState.yes)
				ret |= ModNumber.HardRock;
			if (fl == TriState.yes)
				ret |= ModNumber.Flashlight;
			return ret;
		}

		ModNumber disallowedMods()
		{
			ModNumber ret;
			if (dt == DT.no)
				ret |= ModNumber.HalfTime | ModNumber.DoubleTime;
			if (hd == TriState.no)
				ret |= ModNumber.Hidden;
			if (hr == TriState.no)
				ret |= ModNumber.HardRock;
			if (fl == TriState.no)
				ret |= ModNumber.Flashlight;
			return ret;
		}

		static bool matchesFilter(ModNumber mods, ModNumber required, ModNumber disallowed)
		{
			return (mods & disallowed) == 0 && (mods | ~required) == cast(ModNumber) uint.max;
		}
	}

	alias Recipe = SchemaVariant!(Pick, "manual", AutoPPPick, "autopp");

	struct Pick
	{
		BsonObjectID map;
		string comment;
		Mod[] mods;
		GameMode mode;

		void toRecipe(ref Appender!string dst) const
		{
			dst.put(Map.findById(map).mapID.to!string);
			switch (mode)
			{
			case GameMode.osu:
				dst.put(',');
				dst.put("osu");
				break;
			case GameMode.taiko:
				dst.put(',');
				dst.put("taiko");
				break;
			case GameMode.ctb:
				dst.put(',');
				dst.put("ctb");
				break;
			case GameMode.mania:
				dst.put(',');
				dst.put("mania");
				break;
			default:
				break;
			}

			if (mods.length)
			{
				dst.put("+");
				foreach (mod; mods)
					dst.put(mod.shortForm);
			}

			if (comment.length)
			{
				dst.put(": ");
				dst.put(comment);
			}
		}
	}

	Pick[] picks;
	Recipe[] recipe;
	SchemaDate created = SchemaDate.now;
	BsonObjectID author;
	string name, description;
	bool deleted;
	bool public_;
	bool featured;
	long viewed, used, plays, likes;

	Json serializeAPI(BsonObjectID user, bool admin) const
	{
		Json ret = Json.emptyObject;
		ret["id"] = Json(frontendID);
		ret["name"] = Json(name);
		ret["description"] = Json(description);
		ret["author"] = Json(author.toString);
		ret["created"] = Json(created.toISOExtString);
		ret["recipe"] = Json(recipeString);
		ret["featured"] = Json(featured);
		ret["viewed"] = Json(viewed);
		ret["used"] = Json(used);
		ret["plays"] = Json(plays);
		ret["likes"] = Json(likes);

		if (isAdminOf(user, admin))
		{
			ret["public"] = Json(public_);
		}

		if (admin)
		{
			ret["deleted"] = Json(deleted);
		}
		return ret;
	}

	void didView()
	{
		Playlist.update(["_id": bsonID], ["$inc": ["viewed": 1]]);
	}

	void didUse()
	{
		Playlist.update(["_id": bsonID], ["$inc": ["used": 1]]);
	}

	void didPlay()
	{
		Playlist.update(["_id": bsonID], ["$inc": ["plays": 1]]);
	}

	static void didPlay(BsonObjectID id)
	{
		Playlist.update(["_id": id], ["$inc": ["plays": 1]]);
	}

	void setLikes(int likes)
	{
		Playlist.update(["_id": bsonID], ["$set": ["likes": likes]]);
	}

	void addLikes(int likes)
	{
		Playlist.update(["_id": bsonID], ["$inc": ["likes": likes]]);
	}

	void syncLikes()
	{
		setLikes(cast(int) PlaylistLike.count(query!PlaylistLike.playlist(bsonID)));
	}

	GameMode mode() @property
	{
		if (!recipe.length)
			return GameMode.osu;

		auto first = recipe[0];
		if (first.isType!Pick)
			return first.manual.mode;
		else if (first.isType!AutoPPPick)
			return first.autopp.mode;
		else
			return GameMode.osu;
	}

	string recipeString() @property const
	{
		auto ret = appender!(string);
		foreach (recipe; this.recipe)
		{
			if (recipe.isType!Pick)
				recipe.manual.toRecipe(ret);
			else if (recipe.isType!AutoPPPick)
				recipe.autopp.toRecipe(ret);
			ret.put('\n');
		}
		return ret.data;
	}

	string frontendID() @property @safe const
	{
		import std.ascii : hexDigits;

		char[] ret = new char[27];
		ret[] = '-';
		auto id = cast(const(ubyte)[]) bsonID;

		int offset;
		for (int i = 0; i < 12; i++)
		{
			ret[i * 2 + offset] = hexDigits[(id[i] & 0xF0) >> 4];
			ret[i * 2 + 1 + offset] = hexDigits[id[i] & 0xF];

			if (i % 3 == 2)
			{
				offset++;
			}
		}

		return (() @trusted => cast(string) ret)();
	}

	void build(int limit)
	{
		int totalLimit = limit;
		auto picks = appender!(Pick[]);
		int dynamics;
		foreach (part; recipe)
		{
			if (part.isType!Pick)
				limit--;
			else if (part.isType!AutoPPPick)
				dynamics++;
			else
				assert(false);
		}

		if (limit - dynamics * 1 < 0)
			throw new Exception("Too many maps specified");

		if (dynamics > 4)
			throw new Exception("Too many auto pp generators specified");

		size_t totalGenerated;
		auto generated = appender!(Pick[][]);
		foreach (part; recipe)
		{
			if (part.isType!AutoPPPick)
			{
				auto maps = appender!(Pick[]);
				AutoPPPick filters = part.autopp;
				auto data = getPPFilterData(filters.dataSource);
				bool hasCommentGen = filters.comments.canFind("{{");

				auto requiredMods = filters.requiredMods;
				auto disallowedMods = filters.disallowedMods;

				foreach (PPFilterMap map; data)
				{
					if (map.pp99 >= filters.minPP && map.pp99 <= filters.maxPP
							&& map.overweight >= filters.minOverweight && map.overweight <= filters.maxOverweight
							&& map.length >= filters.minLength && map.length <= filters.maxLength
							&& map.keys >= filters.minKeys && map.keys <= filters.maxKeys
							&& map.bpm >= filters.minBpm && map.bpm <= filters.maxBpm
							&& map.difficulty >= filters.minDiff && map.difficulty <= filters.maxDiff
							&& map.passCount >= filters.minPassCount && map.passCount <= filters.maxPassCount
							&& map.updateHours >= filters.minUpdateHours && map.updateHours <= filters.maxUpdateHours
							&& (filters.langs == MapLanguage.any || map.language == filters.langs) && (filters.genres == MapGenre.any
								|| map.genre == filters.genres)
							&& Playlist.AutoPPPick.matchesFilter(cast(ModNumber) map.mods,
								requiredMods, disallowedMods))
					{
						Mod[] mods;
						if ((cast(ModNumber) map.mods & ModNumber.DoubleTime) != 0)
							mods ~= Mod.DoubleTime;
						else if ((cast(ModNumber) map.mods & ModNumber.HalfTime) != 0)
							mods ~= Mod.HalfTime;
						if ((cast(ModNumber) map.mods & ModNumber.Hidden) != 0)
							mods ~= Mod.Hidden;
						if ((cast(ModNumber) map.mods & ModNumber.HardRock) != 0)
							mods ~= Mod.HardRock;
						if ((cast(ModNumber) map.mods & ModNumber.Flashlight) != 0)
							mods ~= Mod.Flashlight;

						auto dbMap = Map.findOrInsert(map.beatmap);
						maps.put(Pick(dbMap.bsonID, hasCommentGen ? filters.comments.generateComment(dbMap,
								map) : filters.comments, mods, filters.mode));
					}
				}

				generated.put(maps.data);
				totalGenerated += maps.data.length;
			}
		}

		int generatedIndex;
		if (limit - totalGenerated >= 0)
		{
			foreach (part; recipe)
			{
				if (part.isType!AutoPPPick)
				{
					picks.put(generated.data[generatedIndex++]);
				}
				else if (part.isType!Pick)
				{
					picks.put(part.manual);
				}
			}
		}
		else
		{
			double baseRemove = (limit / cast(double) totalGenerated);
			foreach (part; recipe)
			{
				if (part.isType!AutoPPPick)
				{
					auto row = generated.data[generatedIndex++];
					int len = cast(int)(row.length * baseRemove);
					picks.put(row[0 .. len]);
				}
				else if (part.isType!Pick)
				{
					picks.put(part.manual);
				}
			}
		}

		this.picks = picks.data[0 .. min(totalLimit, $)];
	}

	@trusted mixin MongoSchema;

	static DocumentRange!Playlist findMine(BsonObjectID me)
	{
		return Playlist.findRange(query!Playlist.deleted(false).author(me)).sort([
				"created": -1
				]);
	}

	static DocumentRange!Playlist discover()
	{
		return Playlist.findRange(query!Playlist.public_(true).deleted(false))
			.sort(["featured": -1, "created": -1]);
	}

	static DocumentRange!Playlist findFeatured()
	{
		return Playlist.findRange(query!Playlist.public_(true).deleted(false).featured(true));
	}

	static DocumentRange!Playlist findNew()
	{
		return Playlist.findRange(query!Playlist.public_(true).deleted(false)).sort([
				"created": -1
				]);
	}

	static DocumentRange!Playlist findNewAdmin()
	{
		return Playlist.findRange(query!Playlist.deleted(false)).sort([
				"created": -1
				]);
	}

	bool isAdminOf(BsonObjectID user, bool admin) const
	{
		return admin || author == user;
	}

	bool isVisibleFor(BsonObjectID user, bool admin) const
	{
		return public_ || isAdminOf(user, admin);
	}

	bool doLike(BsonObjectID user, bool like)
	{
		auto existing = PlaylistLike.tryFindOne(["u": Bson(user),
				"p": Bson(bsonID)], PlaylistLike.init);
		if (!existing.bsonID.valid && like)
		{
			PlaylistLike obj;
			obj.playlist = bsonID;
			obj.user = user;
			obj.save();
			addLikes(1);
			return true;
		}
		else if (existing.bsonID.valid && !like)
		{
			existing.remove();
			addLikes(-1);
			return true;
		}
		return false;
	}
}

struct PlaylistLike
{
	@schemaName("p")
	BsonObjectID playlist;
	@schemaName("u")
	BsonObjectID user;

	@trusted mixin MongoSchema;
}

struct Map
{
	@mongoForceIndex long mapID;
	long setID;
	SchemaDate fetchDate = SchemaDate.now;
	bool forceUpdate;
	bool deleted;
	@mongoForceIndex int rating;

	void setRating(int rating)
	{
		Map.update(["_id": bsonID], ["$set": ["rating": rating]]);
	}

	void addRating(int rating)
	{
		Map.update(["_id": bsonID], ["$inc": ["rating": rating]]);
	}

	void syncRating()
	{
		auto ret = MapRating.aggregate.match(query!MapRating.map(bsonID))
			.groupAll(["likes": ["$sum": "$v"]]).run();
		setRating(ret["likes"].get!int);
	}

	// automatically updated whenever needsUpdate is true
	@encodeMember("encodeInfo")
	@decodeMember("decodeInfo")
	MapInfo info;

	@trusted mixin MongoSchema;

	Bson encodeInfo(ref Map map)
	{
		Bson info = serializeToBson(map.info);

		info["approvedDate"] = BsonDate(map.info.approvedDate);
		info["lastUpdate"] = BsonDate(map.info.lastUpdate);
		info["hitPlaytime"] = Bson(map.info.hitPlaytime.total!"hnsecs");
		info["playtime"] = Bson(map.info.playtime.total!"hnsecs");
		info["tags"] = serializeToBson(map.info.tags);
		info["md5"] = BsonBinData(BsonBinData.Type.md5, map.info.md5[].idup);

		return info;
	}

	MapInfo decodeInfo(Bson bson)
	{
		MapInfo ret;
		foreach (name, value; bson["info"].get!(Bson[string]))
		{
		MemberSwitch:
			switch (name)
			{
				static foreach (member; FieldNameTuple!MapInfo)
				{
			case member:
					alias T = typeof(__traits(getMember, ret, member));
					static if (is(T == SysTime) || is(T == Duration))
					{
						if (value.type == Bson.Type.object)
						{
							this.forceUpdate = true;
							break MemberSwitch;
						}
					}

					static if (is(T == SysTime))
						__traits(getMember, ret, member) = value.get!BsonDate.toSysTime;
					else static if (is(T == Duration))
						__traits(getMember, ret, member) = value.get!long.hnsecs;
					else static if (is(T == ubyte[16]))
						__traits(getMember, ret, member) = value.get!BsonBinData
							.rawData
							.staticArray!16;
					else
						__traits(getMember, ret, member) = deserializeBson!T(value);

					break MemberSwitch;
				}
			default:
				break;
			}
		}
		return ret;
	}

	bool needsUpdate()
	{
		return !deleted && (forceUpdate || (info.approved == Approval.ranked
				? currentTime - fetchDate.toSysTime > 60.days : currentTime - fetchDate.toSysTime > 14.days));
	}

	static DocumentRange!Map findByPicks(Playlist.Pick[] picks)
	{
		return Map.findRange(["_id": ["$in": picks.map!"a.map".array]]);
	}

	static Map findOrInsert(long mapID) @safe
	{
		mapLookupMutex.lock();
		scope (exit)
			mapLookupMutex.unlock();
		auto existing = mapLookupCache.get(mapID);
		if (!existing.isNull)
			return existing.get;

		Map map = (() @trusted => Map.tryFindOne(query!Map.mapID(mapID).deleted(false), Map.init))();
		if (map == map.init || map.info.beatmapID == 0)
		{
			auto now = MonoTime.currTime;
			if (now < mapLookupContinue)
				sleep(mapLookupContinue - now);
			mapLookupContinue = MonoTime.currTime + 50.msecs;

			logInfo("Fetching mapID %s", mapID);
			auto info = queryMap(mapID.to!string);
			if (info == MapInfo.init)
				throw new Exception(text("Map with id ", mapID, " not found"));

			map.mapID = mapID;
			map.setID = info.setID;
			map.info = info;
			(() @trusted => map.save())();
		}

		mapLookupCache.put(mapID, map);
		return map;
	}

	static DocumentRange!Map findByNeedsUpdate()
	{
		return Map.findRange([
				"$and": [
					["deleted": Bson(false)],
					[
						"$or": Bson([
								serializeToBson(["forceUpdate": Bson(true)]),
								serializeToBson([
									"info.approved": ["$eq": Bson(1)],
									"fetchDate": ["$lte": Bson(BsonDate(currentTime - 60.days))]
								]),
								serializeToBson([
									"info.approved": ["$ne": Bson(1)],
									"fetchDate": ["$lte": Bson(BsonDate(currentTime - 14.days))]
								])
							])
					]
				]
				]);
	}

	bool doRating(BsonObjectID gameuser, int rating)
	{
		auto existing = MapRating.tryFindOne([
				"u": Bson(gameuser),
				"p": Bson(bsonID)
				], MapRating.init);
		if (!existing.bsonID.valid)
		{
			if (rating != 0)
			{
				MapRating obj;
				obj.map = bsonID;
				obj.gameuser = gameuser;
				obj.value = rating;
				obj.save();
				addRating(rating);
				return true;
			}
			else
				return false;
		}
		else
		{
			if (rating == 0)
			{
				existing.remove();
				addRating(-existing.value);
				return true;
			}
			else
			{
				if (existing.value != rating)
				{
					addRating(rating - existing.value);
					MapRating.update(["_id": existing.bsonID], ["$set": ["v": rating]]);
					return true;
				}
				else
					return false;
			}
		}
	}
}

struct MapRating
{
	@schemaName("p")
	BsonObjectID map;
	@schemaName("u")
	BsonObjectID gameuser;
	@schemaName("v")
	int value;

	@trusted mixin MongoSchema;
}

struct GameUser
{
	string userID;
	string username;
	SchemaDate firstJoined;
	string[] nameHistory;
	SchemaDate[] hostLeaves;
	ulong numHostLeavePenalties;
	ulong joins;
	SchemaDate lastJoined;
	ulong startedSkipVotes;
	ulong numSkipVotes;
	ulong startedRetryVotes;
	ulong numRetryVotes;
	ulong numHosts;
	ulong picksGotSkipped;
	SchemaDate[] startedInvalid;
	ulong playStarts;
	ulong playFinishes;
	ulong numMapRatings;

	@trusted mixin MongoSchema;

	void didStart()
	{
		GameUser.update(["_id": bsonID], ["$inc": ["playStarts": 1]]);
	}

	void didFinish()
	{
		GameUser.update(["_id": bsonID], ["$inc": ["playFinishes": 1]]);
	}

	void gotSkipped()
	{
		GameUser.update(["_id": bsonID], ["$inc": ["picksGotSkipped": 1]]);
	}

	void didInvalidStart()
	{
		GameUser.update(["_id": bsonID],
				["$push": ["startedInvalid": SchemaDate.toBson(SchemaDate.now)]]);
	}

	void didJoin()
	{
		GameUser.update(["_id": bsonID], [
				"$inc": ["joins": Bson(1)],
				"$set": ["lastJoined": SchemaDate.toBson(SchemaDate.now)]
				]);
	}

	void didLeaveAsHost()
	{
		GameUser.update(["_id": bsonID], [
				"$push": ["hostLeaves": SchemaDate.toBson(SchemaDate.now)]
				]);
	}

	void voteSkip(bool started)
	{
		if (started)
			GameUser.update(["_id": bsonID], [
					"$inc": ["numSkipVotes": 1, "startedSkipVotes": 1]
					]);
		else
			GameUser.update(["_id": bsonID], ["$inc": ["numSkipVotes": 1]]);
	}

	void voteRetry(bool started)
	{
		if (started)
			GameUser.update(["_id": bsonID], [
					"$inc": ["numRetryVotes": 1, "startedRetryVotes": 1]
					]);
		else
			GameUser.update(["_id": bsonID], ["$inc": ["numRetryVotes": 1]]);
	}

	void didMapRating()
	{
		GameUser.update(["_id": bsonID], ["$inc": ["numMapRatings": 1]]);
	}

	bool shouldGiveHost()
	{
		auto now = Clock.currTime;
		if ((hostLeaves.length && (hostLeaves.length < numHostLeavePenalties
				|| (now - hostLeaves[$ - 1].toSysTime()) < 24.hours)) || (startedInvalid.length > 2
				&& (now - startedInvalid[$ - 2].toSysTime) < 24.hours
				&& (now - startedInvalid[$ - 1].toSysTime) < 12.hours))
		{
			numHostLeavePenalties++;
			save();
			return false;
		}
		else
		{
			numHosts++;
			save();
			return true;
		}
	}

	static GameUser findByUsername(string username)
	{
		auto cache = usernameUserCache.get(username);
		UserInfo user;
		if (cache.isNull)
		{
			user = queryUser(username);
			usernameUserCache.put(username, user);
		}
		else
			user = cache.get;
		auto obj = GameUser.tryFindOne(query!GameUser.userID(user.userID), GameUser.init);
		if (obj.bsonID.valid)
		{
			if (obj.username != user.username)
			{
				obj.nameHistory ~= obj.username;
				obj.username = user.username;
				obj.save();
			}
			return obj;
		}
		GameUser ret;
		ret.userID = user.userID;
		ret.username = user.username;
		ret.firstJoined = SchemaDate.now;
		ret.save();
		return ret;
	}
}

unittest
{
	Map map;
	map.mapID = 20;
	map.setID = 10;
	map.fetchDate = SchemaDate(1000000000L);
	map.forceUpdate = true;
	map.deleted = false;
	map.rating = 4;
	map.info.approved = Approval.loved;
	map.info.approvedDate = BsonDate(1000002000L).toSysTime;
	map.info.lastUpdate = BsonDate(1000001000L).toSysTime;
	map.info.artist = "WebFreakSings";
	map.info.setID = 10;
	map.info.beatmapID = 20;
	map.info.bpm = 180.01;
	map.info.creator = "WebFreak";
	map.info.difficultyRating = 6.7;
	map.info.CS = 5;
	map.info.OD = 7;
	map.info.AR = 9.4;
	map.info.HP = 5.4;
	map.info.hitPlaytime = 90.seconds;
	map.info.source = "Earth";
	map.info.genre = MapGenre.anime;
	map.info.language = MapLanguage.german;
	map.info.title = "My Masterpiece";
	map.info.playtime = 100.seconds;
	map.info.difficultyName = "Extreme";
	map.info.mode = MapMode.osu;
	map.info.tags = ["webfreak", "cool"];
	map.info.numFavorites = 1337;
	map.info.playCount = 9001;
	map.info.passCount = 69;
	map.info.maxCombo = 420;
	map.info.md5 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

	Bson obj = memberToBson(map);
	assert(fromSchemaBson!Map(obj) == map);

	assert(obj.type == Bson.Type.object);
	assert(obj["mapID"].get!long == 20);
	assert(obj["setID"].get!long == 10);
	assert(obj["fetchDate"].get!BsonDate.value == BsonDate(1000000000L).value);
	assert(obj["forceUpdate"].get!bool == true);
	assert(obj["deleted"].get!bool == false);
	assert(obj["rating"].get!int == 4);
	assert(obj["info"]["approved"].get!int == 4);
	assert(obj["info"]["approvedDate"].get!BsonDate.value == BsonDate(1000002000L).value);
	assert(obj["info"]["lastUpdate"].get!BsonDate.value == BsonDate(1000001000L).value);
	assert(obj["info"]["artist"].get!string == "WebFreakSings");
	assert(obj["info"]["setID"].get!long == 10);
	assert(obj["info"]["beatmapID"].get!long == 20);
	assert(obj["info"]["bpm"].get!double == 180.01);
	assert(obj["info"]["creator"].get!string == "WebFreak");
	assert(obj["info"]["difficultyRating"].get!double == 6.7);
	assert(obj["info"]["CS"].get!double == 5);
	assert(obj["info"]["OD"].get!double == 7);
	assert(obj["info"]["AR"].get!double == 9.4);
	assert(obj["info"]["HP"].get!double == 5.4);
	assert(obj["info"]["hitPlaytime"].get!long == 90 * 10_000_000);
	assert(obj["info"]["source"].get!string == "Earth");
	assert(obj["info"]["genre"].get!int == 3);
	assert(obj["info"]["language"].get!int == 8);
	assert(obj["info"]["title"].get!string == "My Masterpiece");
	assert(obj["info"]["playtime"].get!long == 100 * 10_000_000);
	assert(obj["info"]["difficultyName"].get!string == "Extreme");
	assert(obj["info"]["mode"].get!int == 0);
	assert(obj["info"]["tags"].type == Bson.Type.array);
	assert(obj["info"]["tags"].get!(Bson[])[0].get!string == "webfreak");
	assert(obj["info"]["tags"].get!(Bson[])[1].get!string == "cool");
	assert(obj["info"]["numFavorites"].get!long == 1337);
	assert(obj["info"]["playCount"].get!long == 9001);
	assert(obj["info"]["passCount"].get!long == 69);
	assert(obj["info"]["maxCombo"].get!long == 420);
	assert(obj["info"]["md5"].get!BsonBinData == BsonBinData(BsonBinData.Type.md5,
			[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]));
}
