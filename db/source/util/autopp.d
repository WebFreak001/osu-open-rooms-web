module util.autopp;

import bancho.irc;

import vibe.core.core;
import vibe.core.stream;
import vibe.core.path;
import vibe.core.file;

import core.time;

import std.algorithm;
import std.ascii;
import std.conv;
import std.format;
import std.math;
import std.range;
import std.string;
import std.traits;
import std.uni;

import osu.api.map;

import db.game;

import util.caches;

@safe:

void setSource(ref Playlist.AutoPPPick pick, string source)
{
	string error = validateSource(source);
	if (error.length)
		throw new Exception(error);

	int mode = source.startsWith("standard", "taiko", "ctb", "mania");

	pick.mode = cast(GameMode)(mode - 1);
	pick.dataSource = source;
}

string validateSource(string source)
{
	string error;
	if ((error = NativePath.Format.validateDecodedSegment(source)).length
			|| source.indexOfAny("/\\") != -1 || !source.endsWith(".json"))
		return "pp source is not valid name" ~ (error.length ? ": " ~ error : "");
	auto path = NativePath(only(NativePath.Segment("pp_data"), NativePath.Segment(source)));
	if (!path.existsFile)
		return "pp source does not exist";

	int mode = source.startsWith("standard", "taiko", "ctb", "mania");
	if (!mode)
		return "pp source has invalid mode";

	return null;
}

string[] ppDataSourcesCache;
MonoTimeImpl!(ClockType.coarse) ppDataSourceGeneration;

string[] getPPDataSources() @trusted // because of iterateDirectory
{
	auto now = MonoTimeImpl!(ClockType.coarse).currTime;
	if (now - ppDataSourceGeneration < 30.minutes)
		return ppDataSourcesCache;

	auto ret = appender!(string[]);
	foreach (file; iterateDirectory(NativePath("pp_data")))
	{
		if (!file.isDirectory && file.name.endsWith(".json")
				&& file.name.startsWith("standard", "taiko", "ctb", "mania"))
		{
			ret.put(file.name);
		}
	}
	auto data = ret.data;
	data.sort!"a<b";
	ppDataSourcesCache = data;
	ppDataSourceGeneration = now;
	return data;
}

bool setVariable(ref Playlist.AutoPPPick pp, in char[] varname, in char[] value)
{
	alias Parsable = getSymbolsByUDA!(Playlist.AutoPPPick, parsable);

	try
	{
		switch (varname)
		{
			static foreach (var; Parsable)
			{
		case var.stringof.toLower:
				static if (is(typeof(var) : int))
				{
					if (value.all!(a => a.isDigit) && value.length <= 9)
					{
						__traits(getMember, pp, var.stringof) = cast(typeof(var)) value.to!int;
					}
					else
						goto error;
				}
				else static if (is(typeof(var) : double))
				{
					auto val = cast(typeof(var)) value.to!double;
					if (!val.isFinite)
						goto error;
					__traits(getMember, pp, var.stringof) = val;
				}
				else
					static assert(false, "Didn't implement type " ~ typeof(var).stringof);
				return true;
			}
		default:
			return false;
		}
	}
	catch (Exception)
	{
	}
	error:
	throw new Exception(text("Malformed value ", value, " for variable ", varname));
}

Playlist.AutoPPPick.TriState parseTriState(in char[] value, string name)
{
	switch (value.toLower)
	{
	case "yes":
		return Playlist.AutoPPPick.TriState.yes;
	case "no":
		return Playlist.AutoPPPick.TriState.no;
	case "any":
		return Playlist.AutoPPPick.TriState.any;
	default:
		throw new Exception(text("Malformed value ", value, " for ", name));
	}
}

Playlist.AutoPPPick.DT parseDT(in char[] value)
{
	switch (value.toLower)
	{
	case "yes":
		return Playlist.AutoPPPick.DT.yes;
	case "no":
		return Playlist.AutoPPPick.DT.no;
	case "any":
		return Playlist.AutoPPPick.DT.any;
	case "ht":
		return Playlist.AutoPPPick.DT.ht;
	default:
		throw new Exception(text("Malformed value ", value, " for DT"));
	}
}

string generateComment(string comments, Map map, PPFilterMap pp)
{
	/// Comment generator function.
	/// Replaces {{...}} with variable output.
	/// Variables: dt, ht, hd, hr, fl, beatmap, mapset, overweight, pp, artist,
	/// title, version, difficulty, length, bpm, passcount, lastupdate, genre, language
	/// Booleans can be converted to strings by appending `...|yes|no`
	/// Floats can be truncated to n digits with `|n`

	auto braces = comments.indexOf("{{");
	if (braces == -1)
		return comments;

	while (braces != -1)
	{
		braces = comments.indexOf("{{", braces);
		auto end = comments.indexOf("}}", braces);
		if (end == -1)
			break;
		auto data = comments[braces + 2 .. end].strip;
		auto pipe = data.indexOf('|');
		string mods;
		if (pipe != -1)
		{
			mods = data[pipe + 1 .. $].stripLeft;
			data = data[0 .. pipe].stripRight;
		}
		data = data.toLower;

		string insert = null;
		switch (data)
		{
		case "dt":
			insert = filterValue((cast(ModNumber) pp.mods & ModNumber.DoubleTime) != 0, "DT", mods);
			break;
		case "ht":
			insert = filterValue((cast(ModNumber) pp.mods & ModNumber.HalfTime) != 0, "HT", mods);
			break;
		case "hd":
			insert = filterValue((cast(ModNumber) pp.mods & ModNumber.Hidden) != 0, "HD", mods);
			break;
		case "hr":
			insert = filterValue((cast(ModNumber) pp.mods & ModNumber.HardRock) != 0, "HR", mods);
			break;
		case "fl":
			insert = filterValue((cast(ModNumber) pp.mods & ModNumber.Flashlight) != 0, "FL", mods);
			break;
		case "beatmap":
			insert = filterValue(map.mapID, data, mods);
			break;
		case "mapset":
			insert = filterValue(map.setID, data, mods);
			break;
		case "overweight":
			insert = filterValue(pp.overweight, data, mods);
			break;
		case "pp":
			insert = filterValue(pp.pp99, data, mods);
			break;
		case "artist":
			insert = filterValue(map.info.artist, data, mods);
			break;
		case "title":
			insert = filterValue(map.info.title, data, mods);
			break;
		case "version":
			insert = filterValue(map.info.difficultyName, data, mods);
			break;
		case "difficulty":
			insert = filterValue(pp.difficulty, data, mods);
			break;
		case "length":
			insert = filterValue(pp.length, data, mods);
			break;
		case "bpm":
			insert = filterValue(pp.bpm, data, mods);
			break;
		case "passcount":
			insert = filterValue(pp.passCount, data, mods);
			break;
		case "lastupdate":
			insert = filterValue(pp.updateHours, data, mods);
			break;
		case "genre":
			insert = filterValue(pp.genre.to!string, data, mods);
			break;
		case "language":
			insert = filterValue(pp.language.to!string, data, mods);
			break;
		default:
			break;
		}

		if (insert !is null)
		{
			comments = comments[0 .. braces] ~ insert ~ comments[end + 2 .. $];
			braces += insert.length;
		}
		else
			braces = end + 2;
	}
	return comments;
}

private string filterValue(bool data, string varname, string filter)
{
	auto pipe = filter.indexOf('|');
	if (pipe == -1)
		return data ? varname : "";
	return data ? filter[0 .. pipe] : filter[pipe + 1 .. $];
}

private string filterValue(int data, string varname, string filter)
{
	return data.to!string;
}

private string filterValue(string data, string varname, string filter)
{
	return data;
}

private string filterValue(double data, string varname, string filter)
{
	if (filter.length == 1 && filter.all!(a => a.isDigit))
	{
		auto digits = filter.to!int;
		if (digits == 0)
			return (cast(int) data).to!string;
		else
			return format!"%#.*f"(digits, data);
	}
	return data.to!string;
}

PPFilterMap[] getPPFilterData(string source) @trusted
{
	import vibe.data.json : JsonSerializer;
	import std.path : chainPath;
	import std.stdio : File;

	auto error = validateSource(source);
	if (error.length)
		throw new Exception(error);

	ppReadingMutex.lock();
	scope (exit)
		ppReadingMutex.unlock();

	auto existing = ppReadingCache.get(source);
	if (!existing.isNull)
		return existing.get.maps;

	ubyte[1024] buffer;
	PPJsonAnalyzer jsonStarts;
	jsonStarts.load();

	auto f = File(chainPath("pp_data", source), "rb");

	foreach (chunk; f.byChunk(buffer[]))
		jsonStarts.feed(chunk);

	yield();

	auto starts = jsonStarts.starts.data;
	auto maps = PPFilterData.make(starts.length);
	if (maps == null)
		throw new Exception("Not enough memory to filter maps");
	size_t mapIndex;

	foreach (start; starts)
	{
		PPJsonObjectReader object;
		f.seek(start);

		foreach (chunk; f.byChunk(buffer[]))
		{
			const ret = object.feed(chunk);
			if (ret != PPFilterMap.init)
			{
				maps.ptr[mapIndex++] = ret;
				break;
			}
		}

		yield();
	}

	ppReadingCache.put(source, new PPFilterData(maps));
	return maps;
}

struct PPJsonAnalyzer
{
@safe nothrow:

	Appender!(size_t[]) starts;
	size_t index;

	int depth = 0;
	bool escape;
	bool inString;

	void load() @trusted
	{
		starts.reserve(1024 * 16);
		index = 0;
	}

	void feed(const(ubyte)[] chunk)
	{
		foreach (c; chunk)
		{
			if (inString)
			{
				if (escape)
				{
					escape = false;
				}
				else
				{
					switch (c)
					{
					case '"':
						inString = false;
						break;
					case '\\':
						escape = true;
						break;
					default:
						break;
					}
				}
			}
			else
			{
				switch (c)
				{
				case '{':
					if (depth == 0)
						starts.put(index);
					depth++;
					break;
				case '}':
					depth--;
					break;
				case '"':
					inString = true;
					break;
				default:
					break;
				}
			}
			index++;
		}
	}
}

struct PPJsonObjectReader
{
@safe:
	PPFilterMap ret;

	int depth;

	string key;
	bool inKey, inNumValue;
	bool trackKey;

	bool inString;
	bool escape;

	ubyte[256] valueBuf;
	ubyte valueIndex;

	PPFilterMap feed(const(ubyte)[] chunk)
	{
		foreach (c; chunk)
		{
			if (inKey || inString)
			{
				if (escape)
				{
					escape = false;
				}
				else
				{
					switch (c)
					{
					case '\\':
						trackKey = false;
						escape = true;
						break;
					case '"':
						finishKey();
						inString = false;
						break;
					default:
						if (trackKey && inKey)
						{
							valueBuf[valueIndex++] = c;
						}
						break;
					}
				}
			}
			else
			{
				switch (c)
				{
				case '{':
					finishValue();
					depth++;
					break;
				case '}':
					finishValue();
					depth--;
					if (depth == 0)
						return ret;
					break;
				case '"':
					if (inNumValue || depth != 1)
					{
						inString = true;
						inNumValue = false;
						trackKey = false;
					}
					else
					{
						inKey = true;
						trackKey = true;
						valueIndex = 0;
					}
					break;
				case ':':
					finishValue();
					if (depth == 1)
						inNumValue = true;
					break;
				case ',':
					finishValue();
					break;
				case '0': .. case '9':
				case '.':
					valueBuf[valueIndex++] = c;
					break;
				case ' ':
				case '\r':
				case '\n':
				case '\t':
					if (valueIndex != 0)
						finishValue();
					break;
				default:
					break;
				}
			}
		}
		return PPFilterMap.init;
	}

	void finishKey()
	{
		if (inKey)
		{
			const keydat = valueBuf[0 .. valueIndex];
		Switch:
			switch (cast(const(char)[]) keydat)
			{
				static foreach (known; AliasSeq!("b", "pp99", "m", "bpm", "l", "k",
						"d", "x", "p", "h", "ln", "g"))
				{
			case known:
					key = known;
					break Switch;
				}
			default:
				break;
			}
			inKey = false;
			valueIndex = 0;
		}
	}

	void finishValue()
	{
		if (inNumValue && valueIndex > 0)
		{
			const value = valueBuf[0 .. valueIndex];
			switch (key)
			{
			case "b":
				ret.beatmap = value.asInt;
				break;
			case "pp99":
				ret.pp99 = value.asInt;
				break;
			case "m":
				ret.mods = value.asInt;
				break;
			case "bpm":
				ret.bpm = value.asInt;
				break;
			case "l":
				ret.length = value.asInt;
				break;
			case "k":
				ret.keys = value.asInt;
				break;
			case "d":
				ret.difficulty = value.asFloat;
				break;
			case "x":
				ret.overweight = value.asFloat;
				break;
			case "p":
				ret.passCount = value.asInt;
				break;
			case "h":
				ret.updateHours = value.asInt;
				break;
			case "ln":
				ret.language = cast(MapLanguage) value.asInt;
				break;
			case "g":
				ret.genre = cast(MapGenre) value.asInt;
				break;
			default:
				break;
			}
			key = null;
		}

		valueIndex = 0;
		inNumValue = false;
	}
}

private int asInt(const(ubyte)[] data) @trusted pure
{
	auto dot = data.countUntil('.');
	if (dot == -1)
		return (cast(char[]) data).to!int;
	else
		return (cast(char[]) data[0 .. dot]).to!int;
}

private float asFloat(const(ubyte)[] data) @trusted pure
{
	return (cast(char[]) data).to!float;
}

unittest
{
	import std.range : chunks, drop;
	import std.math : abs;

	PPJsonAnalyzer counter;
	counter.load();
	auto testdata = representation(`[{"m":0,"b":884617,"x":93.01,"pp99":934.52,"art":"LeaF","t":"Doppelganger","v":"Alter Ego","s":407153,"l":144,"bpm":280,"d":9.28,"p":11389,"h":14936,"g":2,"ln":5,"k":7},{"m":64,"b":1023967,"x":1.15,"pp99":944.32,"art":"LeaF","t":"Doppelganger","v":"jakads' Extra","s":407153,"l":144,"bpm":280,"d":7.56,"p":14860,"h":14936,"g":2,"ln":5,"k":7},{"m":64,"b":1231418,"x":20.31,"pp99":899.01,"art":"Chroma","t":"Hoshi ga Furanai Machi","v":"Meteor Shower // pporse's 7K","s":581509,"l":348,"bpm":165,"d":6.54,"p":4113,"h":8982,"g":10,"ln":5,"k":7},{"m":64,"b":1130570,"x":4.96,"pp99":890.27,"art":"LeaF","t":"Alice in Misanthrope -Ensei Alice-","v":"Alice in Wonderland","s":520399,"l":143,"bpm":154,"d":6.68,"p":14651,"h":18613,"g":2,"ln":3,"k":7},{"m":64,"b":888793,"x":5.96,"pp99":877.55,"art":"void","t":"Just Hold on (To All Fighters)","v":"Resolve","s":409440,"l":260,"bpm":170,"d":6.86,"p":2259,"h":9497,"g":10,"ln":2,"k":7},{"m":64,"b":948777,"x":0.31,"pp99":936.88,"art":"xi","t":"Ascension to Heaven","v":"Elysium","s":421541,"l":298,"bpm":200,"d":7.3,"p":9081,"h":22286,"g":2,"ln":5,"k":7},{"m":64,"b":1684161,"x":0.09,"pp99":941.63,"art":"MAZARE","t":"Mazare Party","v":"JAKARE's 6K Extra","s":650415,"l":231,"bpm":200,"d":6.89,"p":1359,"h":4515,"g":5,"ln":3,"k":6},{"m":64,"b":1701215,"x":3.22,"pp99":861.33,"art":"Lime","t":"Smiling","v":"Be Happy","s":810911,"l":366,"bpm":172,"d":6.78,"p":927,"h":2875,"g":2,"ln":5,"k":7},{"m":64,"b":1120078,"x":0.66,"pp99":790.78,"art":"Umeboshi Chazuke","t":"Panic! Pop'n! Picnic!","v":"Picnic!","s":528031,"l":131,"bpm":190,"d":6.51,"p":6732,"h":8643,"g":2,"ln":5,"k":7}]`);
	foreach (chunk; testdata.chunks(128))
		counter.feed(chunk);

	auto starts = counter.starts.data;
	assert(starts == [1, 169, 342, 542, 743, 926, 1095, 1272, 1432]);

	PPFilterMap map;
	PPJsonObjectReader object;
	foreach (chunk; testdata.drop(1).chunks(128))
	{
		map = object.feed(chunk);
		if (map != PPFilterMap.init)
			break;
	}

	assert(map.beatmap == 884617);
	assert(map.mods == 0);
	assert(abs(map.overweight - 93.01) < 0.001, map.overweight.to!string);
	assert(map.pp99 == 934);
	assert(map.length == 144);
	assert(map.bpm == 280);
	assert(abs(map.difficulty - 9.28) < 0.001);
	assert(map.passCount == 11389);
	assert(map.updateHours == 14936);
	assert(map.genre == cast(MapGenre) 2);
	assert(map.language == cast(MapLanguage) 5);
	assert(map.keys == 7);
}
