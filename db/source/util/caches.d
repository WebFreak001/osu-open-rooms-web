module util.caches;

import db.game;

import bancho.irc;

import core.time;

import vibe.core.core;
import vibe.core.sync;
import vibe.data.bson;

import std.range;
import std.container.array;
import std.datetime.systime;

import osu.api.map;
import osu.api.user;

import cachetools;
import cachetools.containers.hashmap;

Cache2Q!(BsonObjectID, UserCacheInfo) userIdNameCache;
InterruptibleTaskMutex userCacheMutex;

Cache2Q!(long, Map) mapLookupCache;
InterruptibleTaskMutex mapLookupMutex;
MonoTime mapLookupContinue;

Cache2Q!(string, PPFilterData) ppReadingCache;
InterruptibleTaskMutex ppReadingMutex;

Cache2Q!(string, UserInfo) usernameUserCache;

HashMap!(string, BsonObjectID) channelActiveLobbyCache;
HashMap!(BsonObjectID, ChatLog) activeLobbyChatLogCache;

static this()
{
	userIdNameCache = new Cache2Q!(BsonObjectID, UserCacheInfo);
	userIdNameCache.size = 2048;
	userIdNameCache.ttl = 1.hours;

	userCacheMutex = new InterruptibleTaskMutex();

	mapLookupCache = new Cache2Q!(long, Map);
	mapLookupCache.size = 512;
	mapLookupCache.ttl = 2.hours;

	mapLookupMutex = new InterruptibleTaskMutex();
	mapLookupContinue = MonoTime.currTime;

	ppReadingCache = new Cache2Q!(string, PPFilterData);
	ppReadingCache.size = 16;

	usernameUserCache = new Cache2Q!(string, UserInfo);
	usernameUserCache.size = 512;
	usernameUserCache.ttl = 1.hours;

	ppReadingMutex = new InterruptibleTaskMutex();
}

struct UserCacheInfo
{
	string name;
	string id;
}

class ChatLog
{
	import vibe.core.concurrency : send;

@safe:
	ubyte index;
	RoomMessage[cast(int) typeof(index).max + 1] log;
	OsuRoom.Settings.Player[16] players;

	static struct RoomMessage
	{
		long date;
		Message message;

		alias message this;
	}

	static struct DBEvent
	{
		string type;
		BsonObjectID id;
	}

	static struct PlayerEvent
	{
		string type;
		string username;
		int slot;
		Team team;
		bool host;
		bool left;
	}

	Task[] receiveTasks;

	auto range()
	{
		if (log[$ - 1] != RoomMessage.init)
			return chain(log[index .. $], log[0 .. index]);
		else
			return chain(log[0 .. index], cast(RoomMessage[]) null);
	}

	bool empty() @property
	{
		return index == 0 && log[$ - 1] == RoomMessage.init;
	}

	void put(Message message) @trusted
	{
		log[index++] = RoomMessage(Clock.currStdTime, message);

		foreach (task; receiveTasks)
			send(task, message);
	}

	void putMap(BsonObjectID map) @trusted
	{
		foreach (task; receiveTasks)
			send(task, DBEvent("map", map));
	}
}

class PPFilterData
{
@safe @nogc nothrow pure:
	import core.memory : pureMalloc, pureFree;

	~this() @trusted
	{
		if (maps != null)
		{
			pureFree(maps.ptr);
			maps = null;
		}
	}

	this(PPFilterMap[] maps)
	{
		this.maps = maps;
	}

	static PPFilterMap[] make(size_t length) @trusted
	{
		auto data = pureMalloc(length * PPFilterMap.sizeof);
		if (data == null)
			return null;
		return (cast(PPFilterMap*) data)[0 .. length];
	}

	PPFilterMap[] maps;
}

struct PPFilterMap
{
	int beatmap;
	int pp99;
	int mods;
	int bpm;
	int length;
	int keys;
	float difficulty = 0;
	float overweight = 0;
	int passCount;
	int updateHours;
	MapLanguage language;
	MapGenre genre;
}
