module routes.registration;

import vibe.vibe;

import core.sync.mutex;

import std.array;
import std.conv;
import std.datetime.stopwatch : StopWatch;
import std.uni : sicmp;
import std.uri : encodeComponent;

import bancho.irc : isValidOsuUsername;

import botpool : config, oauthClient;

import osu.api.v2.base;
import api = osu.api.v2.user;

import db;

import orw.protocol.types;

string osuAuthorizeURL() @property
{
	auto ret = appender!string;
	ret.put("https://osu.ppy.sh/oauth/authorize?response_type=code&scope=identify&client_id=");
	ret.put(oauthClient.id.to!string);
	ret.put("&redirect_uri=");
	ret.put(oauthClient.redirect.encodeComponent);
	return ret.data;
}

void getLogin(scope HTTPServerRequest req, scope HTTPServerResponse res)
{
	res.redirect(osuAuthorizeURL);
}

void getLogout(scope HTTPServerRequest req, scope HTTPServerResponse res)
{
	res.terminateSession();
	res.redirect(WebRoot);
}

void getAuthCallback(scope HTTPServerRequest sreq, scope HTTPServerResponse sres)
{
	string error = sreq.query.get("error", "");
	enforceHTTP(!error.length, HTTPStatus.unauthorized,
			"You denied the osu! authentication request.");

	string code = sreq.query.get("code", "");
	enforceHTTP(code.length, HTTPStatus.badRequest, "You denied the osu! authentication request.");

	auto accessToken = makeAccessToken(oauthClient.id.to!string,
			oauthClient.secret, oauthClient.redirect, code);

	auto session = sreq.session;
	if (!session)
		session = sres.startSession();

	auto user = api.User.getMe(accessToken.auth);
	auto osuID = user.id.to!string;
	User existing = User.tryFindOne(["osuID": osuID], User.init);
	if (!existing.bsonID.valid)
	{
		existing.osuID = osuID;
		existing.username = user.username;
		existing.resetSession();
		existing.save();
	}
	enforceHTTP(!existing.banned, HTTPStatus.forbidden, "You are banned");

	if (existing.username != user.username)
	{
		existing.username = user.username;
		existing.save();
	}

	session.set("user", existing.bsonID);
	session.set("token", existing.sessionToken);

	sres.redirect("/");
}
