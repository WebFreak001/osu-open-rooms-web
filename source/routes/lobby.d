module routes.lobby;

import vibe.vibe;

import bancho.irc;

import db;

import util.caches;

import std.algorithm;
import std.array;

import master;

void renderLobbies(scope HTTPServerResponse res, User user, bool isAll,
		ref DocumentRange!ActiveLobby lobbies, string search = null)
{
	res.render!("lobbies.dt", user, isAll, lobbies, search);
}

void renderLobbyAdmin(scope HTTPServerResponse res, User user, ActiveLobby lobby, string token,
		OpModeSettingsResponse modeSettings, UserInputException[] errors, string error = null)
{
	import botpool : config;

	string botAccount = config.bancho.username;

	auto chatlog = activeLobbyChatLogCache.get(lobby.bsonID, null);

	auto mode = modeSettings.id;
	auto settings = modeSettings.settings;
	if (mode.id == lobby.lobby.settingsMode)
		settings.base = lobby.lobby.settings.toJson;

	res.render!("lobby_admin.dt", user, lobby, error, token, chatlog,
			botAccount, mode, settings, errors, Map);
}

void sendPacket(T)(ActiveLobby lobby, T packet)
{
	slaveHandler.connection.relay(lobby.pid, T.stringof, serializeToBson(packet));
}

class LobbyInterface
{
	void index(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateUser!false);

		auto lobbies = ActiveLobby.findAll();
		res.renderLobbies(user, true, lobbies);
	}

	void getMine(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateUser!true);

		auto numLobbies = ActiveLobby.countByCreator(user.bsonID);
		if (numLobbies == 0)
			res.redirect(WebRoot);
		else
		{
			auto lobbies = ActiveLobby.findByCreator(user.bsonID);

			if (numLobbies == 1 && !lobbies.empty)
				res.redirect(WebRoot ~ "/lobby/" ~ lobbies.front.bsonID.toString);
			else
				res.renderLobbies(user, false, lobbies);
		}
	}

	private ActiveLobby resolveLobby(User user, string _id)
	{
		auto lobby = ActiveLobby.tryFindById(_id, ActiveLobby.init);
		if (!lobby.bsonID.valid || (!user.admin && lobby.lobby.creator != user.bsonID))
			return ActiveLobby.init;
		return lobby;
	}

	@path("/lobby/:id")
	void getLobby(scope HTTPServerRequest req, scope HTTPServerResponse res,
			string _id, string mode = null)
	{
		mixin(authenticateUser!true);
		auto lobby = resolveLobby(user, _id);
		if (!lobby.bsonID.valid)
			return res.redirect(WebRoot);

		string token = makeToken(req, res);

		string error;
		OpModeSettingsResponse modeSettings;
		try
		{
			modeSettings = slaveHandler.requestModeSettings(slaveHandler.slaveOfPid(lobby.pid),
					lobby.lobby.settingsMode);
		}
		catch (Exception e)
		{
			error = e.msg;
		}

		res.renderLobbyAdmin(user, lobby, token, modeSettings, null, error);
	}

	@path("/lobby/:id/close")
	void getLobbyClose(scope HTTPServerRequest req, scope HTTPServerResponse res,
			string _id, string token)
	{
		mixin(authenticateUser!true);
		auto lobby = resolveLobby(user, _id);
		if (!lobby.bsonID.valid)
			return res.redirect(WebRoot);

		auto modeSettings = slaveHandler.requestModeSettings(slaveHandler.slaveOfPid(lobby.pid),
				lobby.lobby.settingsMode);

		if (!validateToken(req, token))
		{
			token = makeToken(req, res);
			res.statusCode = HTTPStatus.unauthorized;
			res.renderLobbyAdmin(user, lobby, token, modeSettings, null,
					"Invalid request token (try refreshing the page).");
			return;
		}

		lobby.sendPacket(OpClose("Website initiated close", true));

		token = makeToken(req, res);
		res.renderLobbyAdmin(user, lobby, token, modeSettings, null,
				"The lobby will be closed after the next map.");
	}

	@path("/lobby/:id/chat")
	void getLobbyChat(scope HTTPServerRequest req, scope HTTPServerResponse res, string _id)
	{
		mixin(authenticateUser!true);
		auto lobby = resolveLobby(user, _id);
		if (!lobby.bsonID.valid)
			return;

		auto log = activeLobbyChatLogCache.get(lobby.bsonID, null);
		if (log is null)
		{
			channelActiveLobbyCache.put(lobby.lobby.apiId, lobby.bsonID);
			activeLobbyChatLogCache.put(lobby.bsonID, log = new ChatLog);
		}

		handleWebSocket(delegate(scope websocket) @safe nothrow{
			try
			{
				auto task = Task.getThis();
				log.receiveTasks ~= task;
				scope (exit)
					log.receiveTasks = log.receiveTasks.remove!(a => a == task);

				void send(T)(string type, T data)
				{
					if (websocket.connected)
					{
						try
						{
							websocket.send(Json([
									"type": Json(type),
									"data": serializeToJson(data)
								]).toString);
						}
						catch (Exception e)
						{
							websocket.close();
						}
					}
				}

				while (websocket.connected)
				{
					(() @trusted => receiveTimeout(10.seconds, (bancho.irc.Message msg) {
							send("chat", [
								"time": Clock.currTime().toISOExtString,
								"sender": msg.sender,
								"message": msg.message
							]);
						}, (ChatLog.DBEvent event) {
							if (event.type == "map")
								send(event.type, Map.findById(event.id).info);
						}))();
				}
			}
			catch (Exception e)
			{
				logError("Error in chat task: %s", (() @trusted => e.toString)());
			}
		}, req, res);
	}

	@path("/lobby/:id/timings")
	void postLobbyTimings(scope HTTPServerRequest req, scope HTTPServerResponse res,
			string _id, string token, string startGameDuration,
			string allReadyStartDuration, string manualStartDuration,
			string retryGameDuration, string retryEvaluationDuration)
	{
		mixin(authenticateUser!true);
		auto lobby = resolveLobby(user, _id);
		if (!lobby.bsonID.valid)
			return res.redirect(WebRoot);

		if (!validateToken(req, token))
		{
			token = makeToken(req, res);
			res.statusCode = HTTPStatus.unauthorized;

			auto modeSettings = slaveHandler.requestModeSettings(slaveHandler.slaveOfPid(lobby.pid),
					lobby.lobby.settingsMode);

			res.renderLobbyAdmin(user, lobby, token, modeSettings, null,
					"Invalid request token (try refreshing the page).");
			return;
		}

		lobby.sendPacket(OpRoomUpdateDurationsRequest(SerializableDuration.fromString(startGameDuration),
				SerializableDuration.fromString(allReadyStartDuration),
				SerializableDuration.fromString(manualStartDuration), SerializableDuration.fromString(retryGameDuration),
				SerializableDuration.fromString(retryEvaluationDuration)));

		res.redirect(WebRoot ~ "/lobby/" ~ _id);
	}

	@path("/lobby/:id/mode")
	void postLobbyMode(scope HTTPServerRequest req, scope HTTPServerResponse res,
			string _id, string token, string mode)
	{
		mixin(authenticateUser!true);
		auto lobby = resolveLobby(user, _id);
		if (!lobby.bsonID.valid)
			return res.redirect(WebRoot);

		auto modeSettings = slaveHandler.requestModeSettings(slaveHandler.slaveOfPid(lobby.pid), mode);

		if (!validateToken(req, token))
		{
			token = makeToken(req, res);
			res.statusCode = HTTPStatus.unauthorized;

			res.renderLobbyAdmin(user, lobby, token, modeSettings, null,
					"Invalid request token (try refreshing the page).");
			return;
		}

		if (!modeSettings.valid)
		{
			token = makeToken(req, res);
			res.statusCode = HTTPStatus.notFound;

			res.renderLobbyAdmin(user, lobby, token, modeSettings, null,
					"The selected mode could not be found.");
			return;
		}

		token = makeToken(req, res);

		auto errors = modeSettings.settings.fromHTTP(req.form);

		foreach (ModeSetting setting; modeSettings.settings.settings)
		{
			if (setting.type == MapList.stringof)
			{
				auto id = BsonObjectID.fromString(setting.read(modeSettings.settings.base).get!string);

				if (Playlist.count(["_id": id]) == 0)
				{
					errors ~= new UserInputException("This playlist was not found.", setting.id);
				}
			}
		}

		if (errors.length)
		{
			token = makeToken(req, res);
			res.statusCode = HTTPStatus.notFound;

			res.renderLobbyAdmin(user, lobby, token, modeSettings, errors, "Please correct your input.");
			return;
		}

		lobby.lobby.settingsMode = mode;
		lobby.lobby.settings = modeSettings.settings.base;
		lobby.save();

		lobby.sendPacket(OpRoomModeSettings(mode, modeSettings.settings.base));

		res.redirect(WebRoot ~ "/lobby/" ~ _id);
	}
}
