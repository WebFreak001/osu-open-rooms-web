module routes.admin;

import vibe.vibe;

import std.conv;

import db;

import master;
import lobbyqueue;

void renderAdmin(scope HTTPServerResponse res, User user)
{
	res.render!("admin.dt", user);
}

void renderAdminUsers(scope HTTPServerResponse res, User user, string token,
		ref DocumentRange!User users)
{
	res.render!("admin_users.dt", user, users, token);
}

void renderAdminSlaves(scope HTTPServerResponse res, User user, string token)
{
	res.render!("admin_slaves.dt", user, token, slaveHandler, queuedLobbies, slaves);
}

@path("/admin")
class AdminInterface
{
	void index(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateAdmin);

		res.renderAdmin(user);
	}

	void getUsers(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateAdmin);

		string token = makeToken(req, res);

		auto users = User.findAll;
		res.renderAdminUsers(user, token, users);
	}

	void getSlaves(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateAdmin);

		string token = makeToken(req, res);

		res.renderAdminSlaves(user, token);
	}

	@path("/user/mod")
	void getMakeAdmin(scope HTTPServerRequest req, scope HTTPServerResponse res,
			string uid, string token)
	{
		mixin(authenticateAdmin);

		if (!validateToken(req, token))
		{
			res.redirect(WebRoot ~ "/admin/users");
			return;
		}

		auto userObj = User.tryFindById(uid, User.init);
		if (userObj.bsonID.valid && userObj.bsonID != user.bsonID)
		{
			userObj.admin = !userObj.admin;
			userObj.save();
		}
		res.redirect(WebRoot ~ "/admin/users");
	}

	@path("/user/ban")
	void getBanUser(scope HTTPServerRequest req, scope HTTPServerResponse res, string uid,
			string token)
	{
		mixin(authenticateAdmin);

		if (!validateToken(req, token))
		{
			res.redirect(WebRoot ~ "/admin/users");
			return;
		}

		auto userObj = User.tryFindById(uid, User.init);
		if (userObj.bsonID.valid && userObj.bsonID != user.bsonID)
		{
			userObj.banned = !userObj.banned;
			userObj.save();
		}
		res.redirect(WebRoot ~ "/admin/users");
	}

	@path("/slaves/:pid/shutdown")
	void getQueueShutdown(scope HTTPServerRequest req, scope HTTPServerResponse res,
			string _pid, string token)
	{
		mixin(authenticateAdmin);

		if (!validateToken(req, token))
		{
			res.redirect(WebRoot ~ "/admin/slaves");
			return;
		}

		slaveHandler.queueShutdown(slaveHandler.slaveOfPid(_pid.to!int));

		res.redirect(WebRoot ~ "/admin/slaves");
	}
}
