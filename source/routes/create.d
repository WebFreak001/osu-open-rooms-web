module routes.create;

import vibe.vibe;

import bancho.irc;

import std.algorithm;
import std.array;
import std.typecons;

import db;

import lobbyqueue;
import master;
import botpool : config;

import webconfig;

string[] availableMapModes;

void renderCreate(scope HTTPServerResponse res, User user, string token,
		string settings, string error = null)
{
	res.render!("create.dt", user, settings, token, error);
}

void renderConfig(scope HTTPServerResponse res, User user, string token, CreateOptionsEx createOptions,
		ModeSettings settings, UserInputException[] errors = null, string error = null)
{
	res.render!("create_config.dt", user, createOptions, token, settings, errors, error, Playlist);
}

void renderQueue(scope HTTPServerResponse res, User user, int place)
{
	res.render!("queue.dt", user, place);
}

bool validateUserArgument(string name)
{
	import std.utf : validate, UTFException;
	import std.regex : matchFirst;

	if (!name.length)
		return true;

	try
	{
		validate(name);
		return !!matchFirst(name, `^[\p{L}\p{N}\p{P}\p{S} ]+$`);
	}
	catch (UTFException)
	{
		return false;
	}
}

@path("/create")
class CreateInterface
{
	private SessionVar!(CreateOptionsEx, "lobby_settings") _lobbySettings = CreateOptionsEx.init;

	private ulong countOpenLobbies(User user)
	{
		return ActiveLobby.countByCreator(user.bsonID);
	}

	void index(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateUser!true);
		enforceBadRequest(req.session);

		string token = makeToken(req, res);

		CreateOptions createOptions = _lobbySettings.base;
		string settings = renderSettings(createOptions, "", "/create", "POST", null);

		auto lobbies = countOpenLobbies(user);

		if (lobbies > 0 && !user.admin)
		{
			res.renderCreate(user, token, settings,
					"You already have a lobby currently open and can't create another one.");
			return;
		}

		res.renderCreate(user, token, settings);
	}

	@method(HTTPMethod.POST)
	@path("/")
	void postIndex(scope HTTPServerRequest req, scope HTTPServerResponse res,
			int uptime, string mode, string token)
	{
		mixin(authenticateUser!true);

		if (config.limitedMode)
		{
			res.writeBody("Currently in maintenance mode, please try again in a minute.");
			return;
		}

		enforceBadRequest(req.session);

		CreateOptions createOptions = _lobbySettings.base;
		auto err = req.processSettings(createOptions);

		if (!validateToken(req, token))
		{
			string settings = renderSettings(createOptions, err, "", "/create", "POST", null);
			token = makeToken(req, res);
			res.renderCreate(user, token, settings, "Invalid request token (try refreshing the page).");
			return;
		}

		token = makeToken(req, res);

		auto lobbies = countOpenLobbies(user);

		auto error = appender!string;

		if (lobbies > 0 && !user.admin)
			error.put("You already have a lobby currently open and can't create another one. ");
		if (err != CreateOptions_allCorrect)
			error.put("Please fill out the fields correctly. ");
		if (!validateUserArgument(createOptions.name))
			error.put("Please only use letters, numbers, punctuation and symbols in the room name. ");
		if (!validateUserArgument(createOptions.password))
			error.put("Please only use letters, numbers, punctuation and symbols in the password. ");
		if (!createOptions.extraMatchReferees.lineSplitter.all!(a => !a.length
				|| isValidOsuUsername(a)))
			error.put("Please put valid usernames for the extra match referees. ");
		if ((uptime < 10 && !user.admin) || uptime > 360)
			error.put("Please pick a correct uptime. ");

		if (error.data.length)
		{
			string settings = renderSettings(createOptions, err, "", "/create", "POST", null);
			res.renderCreate(user, token, settings, error.data);
			return;
		}

		auto modeSettings = slaveHandler.requestModeSettings(mode);

		if (!modeSettings.valid)
		{
			string settings = renderSettings(createOptions, err, "", "/create", "POST", null);
			res.renderCreate(user, token, settings,
					"Please pick a different map mode. This one could not be found.");
			return;
		}

		_lobbySettings = CreateOptionsEx(SerializableDuration(uptime.minutes),
				createOptions, mode, modeSettings.id.name);
		res.redirect(WebRoot ~ "/create/config");
	}

	void getConfig(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateUser!true);
		enforceBadRequest(req.session);

		string token = makeToken(req, res);

		const CreateOptionsEx createOptionsEx = _lobbySettings;

		CreateOptions createOptions = createOptionsEx.base;
		auto mode = createOptionsEx.mapMode;

		if (createOptions == CreateOptions.init || !createOptions.name.length || !mode.length)
		{
			res.redirect(WebRoot ~ "/create");
			return;
		}

		auto modeSettings = slaveHandler.requestModeSettings(mode);

		if (!modeSettings.valid)
		{
			string settings = renderSettings(createOptions, "", "/create", "POST", null);
			res.renderCreate(user, token, settings,
					"Please pick a different map mode. This one could not be found.");
			return;
		}

		res.renderConfig(user, token, createOptionsEx, modeSettings.settings);
	}

	void postConfig(scope HTTPServerRequest req, scope HTTPServerResponse res, string token)
	{
		mixin(authenticateUser!true);

		if (config.limitedMode)
		{
			res.writeBody("Currently in maintenance mode, please try again in a minute.");
			return;
		}

		auto error = appender!string;

		if (!validateToken(req, token))
			error.put("Invalid request token (try refreshing the page). ");

		token = makeToken(req, res);

		const CreateOptionsEx createOptionsEx = _lobbySettings;

		CreateOptions createOptions = createOptionsEx.base;
		auto mode = createOptionsEx.mapMode;

		if (createOptions == CreateOptions.init || !createOptions.name.length || !mode.length)
		{
			res.redirect(WebRoot ~ "/create");
			return;
		}

		auto modeSettings = slaveHandler.requestModeSettings(mode);

		if (!modeSettings.valid || (!user.admin && createOptionsEx.uptime == Duration.zero))
		{
			string settings = renderSettings(createOptions, "", "/create", "POST", null);
			res.renderCreate(user, token, settings,
					"Please pick a different map mode. This one could not be found.");
			return;
		}

		auto errors = modeSettings.settings.fromHTTP(req.form);

		foreach (ModeSetting setting; modeSettings.settings.settings)
		{
			if (setting.type == MapList.stringof)
			{
				auto id = BsonObjectID.fromString(setting.read(modeSettings.settings.base).get!string);

				if (Playlist.count(["_id": id]) == 0)
				{
					errors ~= new UserInputException("This playlist was not found.", setting.id);
				}
			}
		}

		if (errors.length)
		{
			res.renderConfig(user, token, createOptionsEx, modeSettings.settings,
					errors, "Please correct your input.");
			return;
		}

		auto lobbies = countOpenLobbies(user);

		if (lobbies > 0 && !user.admin)
		{
			string settings = renderSettings(createOptions, "", "/create", "POST", null);
			res.renderCreate(user, token, settings,
					"You already have a lobby currently open and can't create another one.");
			return;
		}

		Json data = modeSettings.settings.base;

		queueLobby(user.bsonID, user.admin ? 1000 : 1, user.username, data,
				createOptionsEx, user.admin);

		res.redirect(WebRoot ~ "/create/queue");
	}

	void getQueue(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateUser!true);

		auto place = queuedLobbies.range.countUntil!(a => a.user == user.bsonID);

		if (place == -1)
		{
			res.redirect(WebRoot ~ "/mine");
		}
		else
		{
			res.renderQueue(user, cast(int) place);
		}
	}

	@path("/queue/poll")
	void getQueuePoll(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateUser!true);

		foreach (lobby; queuedLobbies.range)
		{
			if (lobby.user == user.bsonID)
			{
				auto m = lobby.condition.mutex;
				auto c = lobby.condition;
				m.lock();
				scope (exit)
					m.unlock();

				c.wait(20.seconds);
				break;
			}
		}
		auto place = queuedLobbies.range.countUntil!(a => a.user == user.bsonID);
		res.writeBody((place + 1).to!string, 200, "plain/text");
	}
}
