module routes.api;

import db;

import std.algorithm;
import std.array;
import std.range;

import crypt.password;

import vibe.vibe;

void renderAPI(scope HTTPServerResponse res, User user, string token, string error = null)
{
	res.render!("api.dt", user, token, error);
}

@path("/api")
class APIInterface
{
	void index(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateUser!true);

		string token = makeToken(req, res);

		res.renderAPI(user, token);
	}

	void postRefresh(scope HTTPServerRequest req, scope HTTPServerResponse res, string token)
	{
		mixin(authenticateUser!true);

		if (!validateToken(req, token))
		{
			token = makeToken(req, res);
			res.renderAPI(user, token, "Invalid request token (try refreshing the page).");
			return;
		}
		token = makeToken(req, res);

		user.apiToken = generateKey;
		user.save();

		res.renderAPI(user, token);
	}

	void postRevoke(scope HTTPServerRequest req, scope HTTPServerResponse res, string token)
	{
		mixin(authenticateUser!true);

		if (!validateToken(req, token))
		{
			token = makeToken(req, res);
			res.renderAPI(user, token, "Invalid request token (try refreshing the page).");
			return;
		}
		token = makeToken(req, res);

		user.apiToken = null;
		user.save();

		res.renderAPI(user, token);
	}

	// endpoints

	@path("/admin/maintenance")
	void getMaintenance(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		import botpool : config;

		mixin(authenticateAPIUser!true);
		if (!user.admin)
			return;

		config.limitedMode = !config.limitedMode;

		res.writeJsonBody(config.limitedMode);
	}

	@path("/admin/status/master")
	void getMasterStatus(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		import botpool : config;
		import master : managerConnected;

		mixin(authenticateAPIUser!true);
		if (!user.admin)
			return;

		res.writeJsonBody(managerConnected, managerConnected ? HTTPStatus.ok
				: HTTPStatus.serviceUnavailable);
	}

	@path("/admin/status/slaves")
	void getSlavesStatus(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		import botpool : config;
		import master : slaves;
		import lobbyqueue : queuedLobbies;

		mixin(authenticateAPIUser!true);
		if (!user.admin)
			return;

		Json ret = Json.emptyObject;
		ret["num"] = Json(slaves.length);
		auto numAvailable = slaves.count!"a.available";
		ret["available"] = Json(numAvailable);
		ret["queued"] = Json(queuedLobbies.length);

		res.writeJsonBody(ret, numAvailable ? HTTPStatus.ok : HTTPStatus.serviceUnavailable);
	}

	@path("/maplists")
	void getMaplists(scope HTTPServerRequest req, scope HTTPServerResponse res,
			int start = 0, int limit = 100)
	{
		mixin(authenticateAPIUser!true);

		enforceBadRequest(start >= 0, "Start must be positive or zero");
		enforceBadRequest(limit >= 1 && limit <= 100, "Limit must be in range 1 .. 100");

		auto ret = user.admin ? Playlist.findNewAdmin() : Playlist.findNew();

		res.writeJsonRangeBody(ret.skip(start).limit(limit)
				.map!(a => a.serializeAPI(user.bsonID, user.admin)));
	}

	@path("/maplists/:id")
	void getMaplistDetails(scope HTTPServerRequest req, scope HTTPServerResponse res, string _id)
	{
		mixin(authenticateAPIUser!true);

		auto ret = Playlist.tryFindById(_id.replace("-", "").toLower, Playlist.init);
		if (!ret.bsonID.valid || !ret.isVisibleFor(user.bsonID, user.admin))
		{
			res.statusCode = 404;
			return;
		}

		res.writeJsonBody(ret.serializeAPI(user.bsonID, user.admin));
	}

	@path("/maplists/:id/likes")
	void getMaplistLikes(scope HTTPServerRequest req, scope HTTPServerResponse res, string _id)
	{
		mixin(authenticateAPIUser!true);

		auto ret = Playlist.tryFindById(_id.replace("-", "").toLower, Playlist.init);
		if (!ret.bsonID.valid || !ret.isVisibleFor(user.bsonID, user.admin))
		{
			res.statusCode = 404;
			return;
		}

		res.writeJsonBody(ret.likes);
	}

	@path("/lobbies")
	void getLobbies(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateAPIUser!true);

		res.writeJsonBody(Json(ActiveLobby.findAll().map!(a => serializeToJson(a)).array));
	}

	@path("/lobbies/user")
	void getLobbiesUser(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateAPIUser!true);

		res.writeJsonBody(Json(ActiveLobby.findByCreator(user.bsonID)
				.map!(a => serializeToJson(a)).array));
	}

	@path("/maps/:id")
	void getBeatmapDetails(scope HTTPServerRequest req, scope HTTPServerResponse res, string _id)
	{
		mixin(authenticateAPIUser!true);

		auto map = Map.findOrInsert(_id.to!long);
		if (map.deleted)
		{
			res.statusCode = 404;
			return;
		}

		res.writeJsonBody(toSchemaBson(map).toJson);
	}
}

void writeJsonRangeBody(T)(scope HTTPServerResponse res, T range)
		if (isInputRange!T)
{
	res.headers["Content-Type"] = "application/json; charset=UTF-8";

	scope rng = streamOutputRange!2048(res.bodyWriter);
	rng.put('[');
	if (!range.empty)
	{
		rng.put('\n');
		serializeToJson(() @trusted { return &rng; }(), range.front);
		range.popFront();
		foreach (item; range)
		{
			rng.put(',');
			rng.put('\n');
			serializeToJson(() @trusted { return &rng; }(), item);
		}
		rng.put('\n');
	}
	rng.put(']');
}
