module master;

import core.sync.mutex;
import core.time;

import vibe.core.concurrency;
import vibe.core.core;
import vibe.core.log;
import vibe.core.net;
import vibe.core.sync;
import vibe.data.bson;
import vibe.data.json;

public import orw.protocol.types;

import orw.proto_translate;
import orw.settings;
import orw.tcp;

import std.algorithm;
import std.array;
import std.container.array;
import std.conv;
import std.functional;
import std.random;
import std.range;
import std.typecons;

import cachetools;

import db;
import lobbyqueue;

import util.caches;

import bancho.irc;

class RelayManagerTCPSlaveConnection : TCPSlaveConnection
{
@safe:
	Timer pingTimer;

	this(string host, ushort port, Duration timeout = 20.seconds)
	{
		super(host, port, timeout);
	}

	this(NetworkAddress addr, Duration timeout = 20.seconds)
	{
		super(addr, timeout);
	}

	/// Creates a new TCPSlaveConnection instance and takes control of the passed tcp connection.
	/// The receive loop needs to be started using receiveLoop(). It is run synchroniously.
	this(TCPConnection connection)
	{
		super(connection);
	}

	override void received(string type, Bson data)
	{
		if (type == RelayMessage.stringof)
		{
			auto relay = deserializeBson!RelayMessage(data);
			if (relay.pid == 0)
			{
				logWarn("Discarding %s relay message from PID 0", relay.t);
				return;
			}

			auto index = slaves.countUntil!(a => a.pid == relay.pid);
			if (index == -1)
			{
				if (relay.t == SlaveInfo.stringof)
				{
					auto info = deserializeBson!SlaveInfo(relay.d);
					if (info.pid != relay.pid)
					{
						logError("Received invalid PID. Relay=%s != Info=%s", relay.pid, info.pid);
						return;
					}

					index = slaves.length;
					slaves ~= info;
					slaveHandler.onAdded(cast(int) index);
					logDiagnostic("Slave %s is %s", index, info);
				}
				else if (relay.t == "close")
				{
					// already non-existant
				}
				else
				{
					logError("Received %s relay message for unknown PID %s", relay.t, relay.pid);
				}
			}
			else
			{
				if (relay.t == SlaveInfo.stringof)
				{
					auto info = deserializeBson!SlaveInfo(relay.d);
					if (info.pid != relay.pid)
					{
						logError("Received invalid PID. Relay=%s != Info=%s", relay.pid, info.pid);
						return;
					}

					slaves[index] = info;
					slaveHandler.onUpdated(cast(int) index);
					logDiagnostic("Slave %s is %s", index, info);
				}
				else if (relay.t == "close")
				{
					slaves[index].pid = 0;
					auto slave = slaves[index];
					while (slaves.length && slaves[$ - 1].pid == 0)
						slaves.length--;
					slaveHandler.onRemoved(slave);
					logDiagnostic("Slave %s disconnected", index);
				}
				else
				{
					logDebugV("Got %s relay message", relay.t);
					handlePacket!(PacketDirection.slaveToHost)(slaveHandler, relay.t,
							relay.d, cast(int) index);
				}
			}
		}
		else
		{
			logWarn("Discarding %s message because only allowing RelayMessages", type);
		}
	}

	void pingTask()
	{
		pingTimer = setTimer(15.seconds, wrapTask(&pingLoop), true);
	}

	void close() nothrow
	{
		connection.close();
		if (pingTimer)
			pingTimer.stop();
	}

	private void pingLoop() nothrow
	{
		try
		{
			sendPing();
			runTask({
				if (!waitPong(5.seconds))
				{
					close();
					logError("Didn't get pong after 6 seconds");
				}
			});
		}
		catch (Exception e)
		{
			close();
			logError("Failed to ping: %s", e.msg);
		}
	}

	void relay(int pid, string t, Bson d)
	{
		sendPacket(RelayMessage(pid, t, d));
	}
}

bool managerConnected;
SlaveInfo[] slaves;
SlaveHandler slaveHandler;

auto connectedSlaves() @property
{
	return slaves.filter!(a => a.pid != 0);
}

struct SlaveHandler
{
@safe:
	RelayManagerTCPSlaveConnection connection;
	Appender!(ModeID[]) availableModes = appender!(ModeID[]);

	CacheLRU!(string, OpModeSettingsResponse) modeSettingsCache;

	void load()
	{
		modeSettingsCache = new CacheLRU!(string, OpModeSettingsResponse);
		modeSettingsCache.size = 32;
		modeSettingsCache.ttl = 4.hours;

		(() @trusted => lobbyMutex = new InterruptibleTaskMutex())();

		(() @trusted => setTimer(5.seconds, wrapTask(toDelegate(&queueChecker)), true))();
	}

	void reload()
	{
		(() @trusted => setTimer(10.seconds, wrapTask(toDelegate(&cleanupLobbies)), false))();
	}

	void onAdded(int slave)
	{
		regenAvailableModes();
	}

	void onUpdated(int slave)
	{
		regenAvailableModes();
	}

	void onRemoved(SlaveInfo info)
	{
		int pid = info.pid;

		runTask(() @trusted {
			ActiveLobby.update(query!ActiveLobby.pid(pid)._query, ["pid": 0], UpdateFlags.multiUpdate);
		});

		regenAvailableModes();
	}

	void regenAvailableModes()
	{
		foreach (ModeID removed; availableModes.data.filter!(
				a => !connectedSlaves.map!"a.availableModes"
				.joiner
				.canFind!(m => m.id == a.id)))
		{
			modeSettingsCache.remove(removed.id);
		}

		availableModes.clear();
		foreach (mode; connectedSlaves.map!"a.availableModes".joiner)
			if (!availableModes.data.canFind!(a => a.id == mode.id))
				availableModes.put(mode);
	}

	void onPacket(OpRegister, int)
	{
		logError("Received invalid OpRegister packet");
	}

	void onPacket(OpModeSettingsResponse response, int slave)
	{
		if (!modeSettingsWaiter.put(response.id.id, slaves[slave].pid, response))
			logError("Failed to put mode settings response, no handler registered at time of packet");

		auto existing = modeSettingsCache.get(response.id.id);
		if (existing.isNull)
			modeSettingsCache.put(response.id.id, response);
	}

	void onPacket(OpRoomSubmitResponse response, int slave)
	{
		if (response.created || response.occupied)
		{
			slaves[slave].available = false;
			slaves[slave].gameId = response.gameId;
			slaves[slave].apiId = response.apiId;

			logInfo("Room created for pid %s, notifying queue...", slaves[slave].pid);
		}
		else if (!response.occupied)
		{
			logInfo("Need to make pid %s available again because it's not occupied", slaves[slave].pid);
			slaves[slave].available = true;
		}
		roomSubmitWaiter.put(null, slaves[slave].pid, response);
		logDiagnostic("Queue notified for pid %s", slaves[slave].pid);
	}

	void onPacket(OpClosed close, int slave)
	{
		int pid = slaves[slave].pid;

		runTask(() @trusted {
			foreach (lobby; ActiveLobby.findRange(query!ActiveLobby.pid(pid)))
				removeActiveLobby(lobby, close.reason);
		});

		slaves[slave].available = true;
		slaves[slave].gameId = null;
		slaves[slave].apiId = null;
	}

	void onPacket(OpSettings settings, int slave)
	{
		logInfo("[%04x] Received settings: %s", slave, settings);
	}

	void onPacket(OpPlayerChange player, int slave)
	{
		logInfo("[%04x] Player change: %s", slave, player);
	}

	void onPacket(OpHostCleared, int slave)
	{
	}

	void onPacket(OpPlayerMessage message, int slave)
	{
		logDiagnostic("[%04x] Player message: %s", slave, message);

		if (message.room.startsWith("#mp_"))
		{
			auto log = activeLobbyChatLogCache.get(channelActiveLobbyCache.get(message.room[4 .. $],
					BsonObjectID.init), null);
			if (log)
				log.put(Message(message.name, message.room, message.message));
		}
	}

	void onPacket(OpMatchStart packet, int slave)
	{
		logInfo("[%04x] Match start: %s", slave, packet);
	}

	void onPacket(OpMatchFinish packet, int slave)
	{
		logInfo("[%04x] Match finish: %s", slave, packet);
	}

	void onPacket(OpBeatmapPending packet, int slave)
	{
		logInfo("[%04x] Beatmap pending: %s", slave, packet);
	}

	void onPacket(OpBeatmapChanged packet, int slave) @trusted
	{
		logDiagnostic("[%04x] Beatmap changed: %s", slave, packet);

		long mapId = packet.map.id.to!long;
		auto activeLobby = channelActiveLobbyCache.get(slaves[slave].apiId, BsonObjectID.init);

		auto map = Map.findOrInsert(mapId);
		if (!map.bsonID.valid)
			return;

		auto lobby = ActiveLobby.tryFindById(activeLobby, ActiveLobby.init);
		if (lobby.bsonID.valid)
		{
			lobby.song = map.bsonID;
			lobby.save();
		}

		auto log = activeLobbyChatLogCache.get(activeLobby, null);
		if (log)
			log.putMap(map.bsonID);
	}

	void relay(int pid, string type, Bson data)
	{
		if (!managerConnected)
			throw new Exception("Tried to relay but no manager was connected");

		foreach (ref slave; connectedSlaves)
		{
			if (slave.pid == pid)
			{
				connection.relay(pid, type, data);

				return;
			}
		}
		throw new Exception("Could not find slave with PID " ~ pid.to!string);
	}

	void sendMessage(int pid, string user, string message)
	{
		relay(pid, OpSendPrivateMessage.stringof,
				serializeToBson(OpSendPrivateMessage(user, message)));
	}

	OpModeSettingsResponse requestModeSettings(string mode)
	{
		auto cache = modeSettingsCache.get(mode);
		if (!cache.isNull)
			return cache.get;

		foreach (pair; slaves.enumerate.randomCover)
			if (pair[1].pid != 0 && pair[1].availableModes.canFind!(a => a.id == mode))
				return requestModeSettings(cast(int) pair[0], mode);
		return OpModeSettingsResponse.init;
	}

	OpModeSettingsResponse requestModeSettings(int slave, string mode)
	{
		auto cache = modeSettingsCache.get(mode);
		if (!cache.isNull)
			return cache.get;

		int pid = slaves[slave].pid;
		runTask({
			connection.relay(pid, OpModeSettingsRequest.stringof,
				serializeToBson(OpModeSettingsRequest(mode)));
		});
		auto ret = modeSettingsWaiter.wait(mode, pid);
		modeSettingsCache.put(mode, ret);
		return ret;
	}

	OpRoomSubmitResponse submitRoom(int slave, OpRoomSubmitRequest request)
	{
		const pid = slaves[slave].pid;
		logInfo("RoomSubmitRequest with request %s", request);
		connection.relay(pid, OpRoomSubmitRequest.stringof, serializeToBson(request));
		auto ret = roomSubmitWaiter.wait(null, pid, 30.seconds);
		if (ret.created)
			connection.relay(pid, OpRoomSubmitAccept.stringof,
					serializeToBson(OpRoomSubmitAccept(ret.gameId)));
		return ret;
	}

	void configRoom(int slave, string mode, Json config)
	{
		connection.relay(slaves[slave].pid, OpRoomModeSettings.stringof,
				serializeToBson(OpRoomModeSettings(mode, config)));
	}

	void queueShutdown(int slave)
	{
		slaves[slave].queuedShutdown = true;
		connection.relay(slaves[slave].pid, QueueShutdown.stringof, Bson.emptyObject);
	}

	int slaveOfPid(int pid)
	{
		foreach (i, ref slave; slaves)
			if (slave.pid && slave.pid == pid)
				return cast(int) i;

		throw new Exception("Slave not found");
	}

	bool hasPid(int pid)
	{
		if (pid == 0)
			return false;

		foreach (i, ref slave; slaves)
			if (slave.pid && slave.pid == pid)
				return true;

		return false;
	}
}

mixin ResponseWaiter!(string, OpModeSettingsResponse) modeSettingsWaiter;
mixin ResponseWaiter!(string, OpRoomSubmitResponse) roomSubmitWaiter;

mixin template ResponseWaiter(Identificator, ReceivePacket)
{
	import core.memory : pureMalloc;

	struct AnswerList
	{
		AnswerQueue data;
		AnswerList* next;
	}

	struct AnswerQueue
	{
		int interested;
		bool got;
		ReceivePacket data;
	}

	struct Job
	{
		Identificator id;
		AnswerQueue* response;
		int pid;
		InterruptibleTaskCondition condition;
	}

	__gshared InterruptibleTaskMutex mutex;
	__gshared AnswerList answerQueue;
	__gshared Array!Job jobs;

	shared static this()
	{
		mutex = new InterruptibleTaskMutex();
	}

	bool put(Identificator id, int pid, ReceivePacket packet) @trusted
	{
		mutex.lock();
		scope (exit)
			mutex.unlock();

		auto i = jobs[].countUntil!(a => a.id == id && a.pid == pid);
		if (i == -1)
		{
			logDiagnostic("Discarding unknown ResponseWaiter.put response for id %s for pid %s: %s",
					id, pid, packet);
			return false;
		}

		jobs[i].response.data = packet;
		jobs[i].response.got = true;
		jobs[i].condition.notifyAll();
		jobs.linearRemove(jobs[i .. i + 1]);
		return true;
	}

	ReceivePacket wait(Identificator id, int pid, Duration timeout = 20.seconds) @trusted
	{
		mutex.lock();
		scope (exit)
			mutex.unlock();

		InterruptibleTaskCondition condition;
		AnswerQueue* response;

		auto i = jobs[].countUntil!(a => a.id == id && a.pid == pid);
		if (i != -1)
		{
			condition = jobs[i].condition;
			response = jobs[i].response;
			response.interested++;
		}
		else
		{
			AnswerList* check = &answerQueue;
			while (check)
			{
				if (check.data.got && check.data.interested <= 0)
				{
					break;
				}

				if (!check.next)
				{
					check.next = cast(AnswerList*) pureMalloc(AnswerList.sizeof);
					if (!check.next)
						assert(false, "out of memory failure");

					check = check.next;
					break;
				}
				else
					check = check.next;
			}

			check.data.got = false;
			check.data.interested = 1;
			check.data.data = ReceivePacket.init;

			jobs.insert(Job(id, response = &check.data, pid,
					condition = new InterruptibleTaskCondition(cast(Lockable) mutex)));
		}

		scope (exit)
			response.interested--;

		if (!condition.wait(timeout))
			throw new Exception("Job task condition didn't notify in time.");

		return response.data;
	}
}
