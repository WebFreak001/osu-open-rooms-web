module services.startup;

import vibe.vibe;

import util.autopp;
import util.caches;

import db.game;

import mongoschema;

void startupServices()
{
	// runTask(&indexPPMaps);
}

void indexPPMaps()
{
	auto available = getPPDataSources();
	foreach (ppsource; available)
	{
		logInfo("Loading all pp maps from %s", ppsource);
		foreach (map; getPPFilterData(ppsource))
			if (mapLookupCache.get(map.beatmap).isNull && Map.count(query!Map.mapID(map.beatmap)) == 0)
				Map.findOrInsert(map.beatmap);
	}
}
