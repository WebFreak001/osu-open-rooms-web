module services.scheduled;

import core.time;

import std.algorithm;
import std.conv;
import std.datetime.systime;
import std.datetime.timezone;

import orw.protocol.serialize;

import db;
import master;

import mongoschema;

struct ScheduledTask
{
	Duration delay;
	bool periodic = true;
}

ScheduledTask scheduled(Duration delay)
{
	return ScheduledTask(delay);
}

void registerScheduledServices()
{
	import std.functional : toDelegate;
	import std.traits : getUDAs, hasUDA;
	import vibe.core.core : setTimer;

	foreach (fn; __traits(allMembers, mixin(__MODULE__)))
	{
		static if (__traits(compiles, mixin(fn)) && hasUDA!(mixin(fn), ScheduledTask))
		{
			enum timers = getUDAs!(mixin(fn), ScheduledTask);
			static foreach (timer; timers)
				setTimer(timer.delay, wrapTask(toDelegate(&mixin(fn))), timer.periodic);
		}
	}
}

@scheduled(2.minutes) void updateMaps()
{
	import db.game : Map;
	import osu.api.map : queryMapset;

	int processed = 0;

	auto maps = Map.findByNeedsUpdate().sort(["setID": -1]);
	while (!maps.empty && processed < 10)
	{
		Map[] toCheck = [maps.front];
		maps.popFront;
		// group all maps of same set
		while (!maps.empty)
		{
			if (maps.front.setID == toCheck[0].setID)
			{
				toCheck ~= maps.front;
				maps.popFront;
			}
			else
				break;
		}

		auto response = queryMapset(toCheck[0].setID.to!string);

		foreach (ref map; toCheck)
		{
			map.forceUpdate = false;
			map.fetchDate = SchemaDate.now;
			processed++;

			auto index = response.countUntil!(m => m.beatmapID == map.mapID);
			if (index == -1)
			{
				map.deleted = true;
			}
			else
			{
				map.info = response[index];
			}

			map.save();
		}
	}
}

@scheduled(2.minutes) void removeDeadLobbies()
{
	auto now = Clock.currTime(UTC());
	foreach (lobby; ActiveLobby.findAll)
	{
		bool expired;
		if (lobby.lobby.ttl.value == Duration.zero)
			expired = !slaveHandler.hasPid(lobby.pid);
		else if ((lobby.lobby.createdAt.toSysTime + lobby.lobby.ttl.value + 3.minutes < now
				&& !slaveHandler.hasPid(lobby.pid))
				|| (lobby.lobby.createdAt.toSysTime + lobby.lobby.ttl.value + 60.minutes < now))
			expired = true;

		if (expired)
		{
			lobby.lobby.save();
			lobby.remove();
		}
	}
}
