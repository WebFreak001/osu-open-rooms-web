module osu.api.config;
@safe:

shared const string banchoApiBase = "https://osu.ppy.sh/api";
shared const string banchoApiKey = "";

shared const string banchoApiV2Base = "https://osu.ppy.sh/api/v2";
