module osu.api.map;
@safe:

import osu.api.config;
import osu.api.utils;

import vibe.data.json;
import vibe.http.client;

import std.array;
import std.conv;
import std.datetime;
import std.string;
import std.uri;

@safe:

enum Approval
{
	graveyard = -2,
	wip,
	pending,
	ranked,
	approved,
	qualified,
	loved
}

enum MapGenre
{
	any,
	unspecified,
	videoGame,
	anime,
	rock,
	pop,
	other,
	novelty = 7,
	hipHop = 9,
	electronic
}

enum MapLanguage
{
	any,
	other,
	english,
	japanese,
	chinese,
	instrumental,
	korean,
	french,
	german,
	swedish,
	spanish,
	italian
}

enum MapMode
{
	osu,
	taiko,
	ctb,
	mania
}

struct MapInfo
{
	Approval approved;
	SysTime approvedDate, lastUpdate, submitDate;
	string artist;
	long setID, beatmapID;
	double bpm = 0;
	string creator;
	long creatorID;
	double difficultyRating;
	double difficultyAim;
	double difficultySpeed;
	double CS = 0, OD = 0, AR = 0, HP = 0;
	Duration hitPlaytime; // play duration without breaks
	string source;
	MapGenre genre;
	MapLanguage language;
	string title;
	Duration playtime; // play duration with breaks
	string difficultyName;
	MapMode mode;
	string[] tags;
	long numFavorites;
	double rating;
	long playCount;
	long passCount;
	long maxCombo;
	long countCircles;
	long countSliders;
	long countSpinners;
	bool downloadUnavailable;
	bool audioUnavailable;
	ubyte[16] md5;
}

MapInfo queryMap(string beatmapID)
{
	auto maps = queryMaps(["b": beatmapID, "limit": "1"]);
	if (maps.length)
		return maps[0];
	else
		return MapInfo.init;
}

MapInfo[] queryMapset(string mapsetID)
{
	return queryMaps(["s": mapsetID]);
}

MapInfo[] queryMaps(string[string] args)
{
	Json ret;

	auto url = appender!string;
	url.put((() @trusted => banchoApiBase)());
	url.put("/get_beatmaps?k=");
	url.put(encodeComponent((() @trusted => banchoApiKey)()));
	foreach (key, value; args)
	{
		url.put('&');
		url.put(encodeComponent(key));
		url.put('=');
		url.put(encodeComponent(value));
	}

	requestHTTP(url.data, (scope req) {}, (scope res) {
		if (res.statusCode != 200)
			throw new Exception("Got non-success " ~ res.statusCode.to!string ~ " result on queryMap");
		ret = res.readJson;
	});
	if (ret.type != Json.Type.array)
		return null;

	auto arr = ret.get!(Json[]);
	auto maps = appender!(MapInfo[]);

	foreach (obj; arr)
		maps.put(jsonToMapInfo(obj));

	return maps.data;
}

MapInfo jsonToMapInfo(Json obj)
{
	MapInfo map;
	map.approved = cast(Approval) obj.tryIndex("approved", "-2").to!int;
	map.approvedDate = parseOsuDate(obj.tryIndex("approved_date", "2000-01-01 00:00:00"));
	map.lastUpdate = parseOsuDate(obj.tryIndex("last_update", "2000-01-01 00:00:00"));
	map.submitDate = parseOsuDate(obj.tryIndex("submit_date", "2000-01-01 00:00:00"));
	map.artist = obj.tryIndex("artist");
	map.beatmapID = obj.tryIndex("beatmap_id", "0").to!long;
	map.setID = obj.tryIndex("beatmapset_id", "0").to!long;
	map.bpm = obj.tryIndex("bpm", "0").to!double;
	map.creator = obj.tryIndex("creator");
	map.creatorID = obj.tryIndex("creator_id", "0").to!long;
	map.difficultyRating = obj.tryIndex("difficultyrating", "0").to!double;
	map.difficultyAim = obj.tryIndex("diff_aim", "0").to!double;
	map.difficultySpeed = obj.tryIndex("diff_speed", "0").to!double;
	map.CS = obj.tryIndex("diff_size", "0").to!double;
	map.OD = obj.tryIndex("diff_overall", "0").to!double;
	map.AR = obj.tryIndex("diff_approach", "0").to!double;
	map.HP = obj.tryIndex("diff_drain", "0").to!double;
	map.hitPlaytime = obj.tryIndex("hit_length", "0").to!double.seconds;
	map.source = obj.tryIndex("source");
	map.genre = cast(MapGenre) obj.tryIndex("genre_id", "1").to!int;
	map.language = cast(MapLanguage) obj.tryIndex("language_id", "1").to!int;
	map.title = obj.tryIndex("title");
	map.playtime = obj.tryIndex("total_length", "0").to!double.seconds;
	map.difficultyName = obj.tryIndex("version");
	map.mode = cast(MapMode) obj.tryIndex("mode", "0").to!int;
	map.tags = obj.tryIndex("tags", "").split(' ');
	map.numFavorites = obj.tryIndex("favourite_count", "0").to!long;
	map.rating = obj.tryIndex("rating", "0").to!double;
	map.playCount = obj.tryIndex("playcount", "0").to!long;
	map.passCount = obj.tryIndex("passcount", "0").to!long;
	map.maxCombo = obj.tryIndex("max_combo", "0").to!long;
	map.countCircles = obj.tryIndex("count_normal", "0").to!long;
	map.countSliders = obj.tryIndex("count_slider", "0").to!long;
	map.countSpinners = obj.tryIndex("count_spinner", "0").to!long;
	map.downloadUnavailable = obj.tryIndex("download_unavailable", "0").to!long != 0;
	map.audioUnavailable = obj.tryIndex("audio_unavailable", "0").to!long != 0;
	map.md5 = obj.tryIndex("file_md5", "").parseMd5;
	return map;
}

private Duration seconds(double d)
{
	return (cast(long)(d * 1000)).msecs;
}

ubyte[16] parseMd5(string s)
{
	if (s.length == 32)
	{
		ubyte[16] ret;
		foreach (i, ref v; ret)
			v = s[i * 2 .. i * 2 + 2].to!ubyte(16);
		return ret;
	}
	else
		return typeof(return).init;
}
