module osu.api.v2.base;
@safe:

import vibe.core.log;
import vibe.data.json;
import vibe.data.serialization;
import vibe.stream.operations;

public import vibe.http.client;

import osu.api.config : banchoApiV2Base;

import core.time;

import std.array;
import std.conv;
import std.datetime.systime;
import std.format;
import std.string;
import std.uri : encodeComponent;

Json requestLazer(HTTPMethod method, string endpoint, string authorization,
		string[string] data = null)
in(endpoint.length && endpoint[0] == '/')
{
	auto query = appender!string;
	string url = banchoApiV2Base ~ endpoint;
	if (data.length)
	{
		bool first = true;
		foreach (k, v; data)
		{
			if (!first)
				query.put('&');
			query.put(encodeComponent(k));
			query.put('=');
			query.put(encodeComponent(v));
			first = false;
		}
	}

	if (data.length && method == HTTPMethod.GET)
		url ~= '?' ~ query.data;

	Json ret;
	requestHTTP(url, (scope req) {
		req.headers.addField("User-Agent", "OpenRooms osu api v2 interface (https://openrooms.app)");

		req.method = method;
		if (authorization.length)
			req.headers.addField("Authorization", authorization);

		if (data.length && method != HTTPMethod.GET)
			req.writeBody(query.data.representation, "application/x-www-form-urlencoded");
	}, (scope res) {
		if (res.statusCode != 200)
		{
			logDiagnostic("Error in %s: status %s, data: %s", endpoint,
				res.statusCode, res.bodyReader.readAllUTF8);
			throw new Exception("API endpoint didn't return ok");
		}

		ret = res.readJson;
	});

	return ret;
}

struct RefreshResponse
{
	@name("token_type")
	string tokenType;

	@name("expires_in")
	long expiresIn;

	@name("access_token")
	string accessToken;

	@name("refresh_token")
	string refreshToken;

@ignore:
	long acquiredAt;

	bool needsRefresh() const @property
	{
		if (expiresIn > 60 * 60)
			return (Clock.currTime - SysTime(acquiredAt) - expiresIn.seconds) > 30.minutes;
		else
			return (Clock.currTime - SysTime(acquiredAt) - expiresIn.seconds) > (expiresIn / 2).seconds;
	}

	string auth() const @property
	{
		return format!"%s %s"(tokenType, accessToken);
	}
}

RefreshResponse makeAccessToken(string clientId, string clientSecret,
		string redirectUri, string code)
{
	RefreshResponse ret;
	requestHTTP(`https://osu.ppy.sh/oauth/token`, (scope req) {
		req.headers.addField("User-Agent", "OpenRooms osu api v2 interface (https://openrooms.app)");

		req.method = HTTPMethod.POST;
		req.writeFormBody([
				"grant_type": "authorization_code",
				"client_id": clientId,
				"client_secret": clientSecret,
				"redirect_uri": redirectUri,
				"code": code
			]);
	}, (scope res) {
		if (res.statusCode != 200)
		{
			if (res.headers.get("Content-Type", "") == "application/json")
			{
				auto err = res.readJson;
				if (err["error"].get!string == "invalid_request")
					throw new HTTPStatusException(HTTPStatus.badRequest, err["hint"].get!string);
			}

			logDiagnostic("Error in makeAccessToken in /oauth/token: status %s, data: %s",
				res.statusCode, res.bodyReader.readAllUTF8);
			throw new Exception("osu! API has returned an invalid status code for authentication");
		}

		ret = res.readJson.deserializeJson!RefreshResponse;
	});
	ret.acquiredAt = Clock.currStdTime;
	return ret;
}

RefreshResponse refreshToken(string clientId, string clientSecret, string refreshToken)
{
	RefreshResponse ret;
	requestHTTP(`https://osu.ppy.sh/oauth/token`, (scope req) {
		req.headers.addField("User-Agent", "OpenRooms osu api v2 interface (https://openrooms.app)");

		req.method = HTTPMethod.POST;
		req.writeFormBody([
				"grant_type": "refresh_token",
				"client_id": clientId,
				"client_secret": clientSecret,
				"refresh_token": refreshToken
			]);
	}, (scope res) {
		if (res.statusCode != 200)
		{
			logDiagnostic("Error in refreshToken in /oauth/token: status %s, data: %s",
				res.statusCode, res.bodyReader.readAllUTF8);
			throw new Exception("Refresh endpoint didn't return ok");
		}

		ret = res.readJson.deserializeJson!RefreshResponse;
	});
	ret.acquiredAt = Clock.currStdTime;
	return ret;
}
