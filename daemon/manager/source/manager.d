module manager;
@safe:

import std.algorithm;
import std.conv;

import core.sync.mutex;
import core.time;

import vibe.core.core;
import vibe.core.net;
import vibe.core.log;
import vibe.core.sync;
import vibe.data.bson;

import orw.protocol.types;
import orw.proto_translate;
import orw.settings;
import orw.tcp;

import bancho.irc;

__gshared TaskMutex relayMutex;
__gshared AdminTCPConnection[] adminRelays;
__gshared RelayMessage[] relayQueue;

__gshared TaskMutex slaveMutex;
__gshared ManagerTCPSlaveConnection[] slaves;

@trusted shared static this()
{
	relayMutex = new TaskMutex;
	slaveMutex = new TaskMutex;
}

void addSlave(ManagerTCPSlaveConnection slave) @trusted
{
	logDebug("Adding slave %s", slave.info);
	synchronized (slaveMutex)
	{
		slaves ~= slave;
	}
}

void removeSlave(ManagerTCPSlaveConnection slave) @trusted
{
	logDebug("Removing slave %s", slave.info);
	synchronized (slaveMutex)
		synchronized (relayMutex)
		{
			slaves = slaves.remove!(a => a == slave);
			if (slave.info.pid)
				foreach_reverse (i, msg; relayQueue)
					if (msg.pid == slave.info.pid)
						relayQueue = relayQueue.remove(i);
		}
	runTask(&slave.gotRemoved);
}

void verifySlavePid(int pid) @trusted
{
	if (pid == 0)
		throw new Exception("Pid 0 can't be registered!");
	synchronized (slaveMutex)
		foreach (slave; slaves)
			if (slave.info.pid == pid)
				throw new Exception("Slave with pid " ~ pid.to!string ~ " already connected!");
	logDebugV("Verified unused slave PID %s", pid);
}

ManagerTCPSlaveConnection getSlaveByPidOrNull(int pid) @trusted
{
	synchronized (slaveMutex)
		foreach (slave; slaves)
			if (pid == DMManagerPid ? (slave.info.manageDMs) : (slave.info.pid == pid))
				return slave;
	return null;
}

shared const Config config;

void initConfig(Config c) @system
{
	cast()config = c;
}

void verifyPassword(string password) @trusted
{
	if (password != config.password)
		throw new Exception("Connection password mismatch");
	logDebug("Verified password");
}

void addAdminRelay(AdminTCPConnection admin) @trusted
{
	synchronized (slaveMutex)
		synchronized (relayMutex)
		{
			foreach (slave; slaves)
				if (slave.info.pid)
					admin.relay(slave.info.pid, SlaveInfo.stringof, serializeToBson(slave.info));
			if (adminRelays.length == 0)
			{
				foreach (queue; relayQueue)
					admin.relay(queue.pid, queue.t, queue.d);
				relayQueue = null;
			}
			adminRelays ~= admin;
		}
}

void removeAdminRelay(AdminTCPConnection admin) @trusted
{
	synchronized (relayMutex)
		adminRelays = adminRelays.remove!(a => a == admin);
}

void relayMessage(int pid, string type, Bson raw) @trusted
{
	synchronized (relayMutex)
	{
		if (adminRelays.length == 0)
			relayQueue ~= RelayMessage(pid, type, raw);
		else
			foreach (ref relay; adminRelays)
				relay.relay(pid, type, raw);
	}
}

__gshared BanchoBot rootUser;

void connectBancho() @trusted
{
	rootUser = new BanchoBot(config.bancho.username, config.bancho.password,
			config.bancho.host, config.bancho.port);
	rootUser.doRatelimit = true;
	runTask({
		while (true)
		{
			rootUser.connect();
			logDiagnostic("Got disconnected from bancho...");
			sleep(2.seconds);
		}
	});
}

class ManagerTCPSlaveConnection : TCPSlaveConnection
{
@safe:
	SlaveInfo info;
	Timer pingTimer;
	bool closed;

	this(string host, ushort port, Duration timeout = 20.seconds)
	{
		super(host, port, timeout);
	}

	this(NetworkAddress addr, Duration timeout = 20.seconds)
	{
		super(addr, timeout);
	}

	/// Creates a new TCPSlaveConnection instance and takes control of the passed tcp connection.
	/// The receive loop needs to be started using receiveLoop(). It is run synchroniously.
	this(TCPConnection connection)
	{
		super(connection);
	}

	void takeOverDMs() @trusted
	{
		synchronized (slaveMutex)
		{
			if (info.pid != 0 && !slaves.any!(a => a.info.pid != 0 && a.info.manageDMs))
			{
				// first slave gets DM ownership
				sendPacket(OpManageDMs());
				info.manageDMs = true;
			}
		}
	}

	override void received(string type, Bson data)
	{
		logDebug("Received packet of type %s from %s", type, info.pid);
		logTrace("%s", type, data);

		if (type == OpRegister.stringof)
		{
			auto register = deserializeBson!OpRegister(data);
			verifyPassword(register.password);
			verifySlavePid(register.pid);
			info = SlaveInfo(register);
			relayMessage(info.pid, SlaveInfo.stringof, serializeToBson(info));
			takeOverDMs();
		}
		else if (info.pid == 0)
		{
			logWarn("Discarding %s message from unregistered slave");
			return;
		}
		else
		{
			if (type == OpRoomSubmitResponse.stringof)
			{
				auto response = deserializeBson!OpRoomSubmitResponse(data);
				if (response.created || response.occupied)
				{
					info.available = false;
					info.gameId = response.gameId;
					info.apiId = response.apiId;
				}
				else if (!response.occupied)
				{
					info.available = true;
				}
			}
			else if (type == OpClosed.stringof)
			{
				info.available = true;
				info.gameId = null;
				info.apiId = null;
			}

			relayMessage(info.pid, type, data);
		}
	}

	void shutdown()
	{
		closed = true;
		if (pingTimer)
			pingTimer.stop();
	}

	private void setupPingTimer()
	{
		logDebug("Setting up ping timer");
		pingTimer = setTimer(10.seconds, &pingLoop, true);
	}

	private void pingLoop() nothrow
	{
		if (!connection.connected)
			return;

		logDebugV("Ping slave %s", info.pid);

		try
		{
			sendPing();
			runTask({
				if (!waitPong(5.seconds))
				{
					if (closed)
						return;
					logInfo("Kicking slave %s for not ponging after 5 seconds", info.pid);
					connection.close();
					if (pingTimer)
						pingTimer.stop();
				}
			});
		}
		catch (Exception e)
		{
			logError("Error while pinging %s: %s", info.pid, e.msg);
		}
	}

	void autoKickTask()
	{
		runTask({
			sleep(5.seconds);
			if (info.pid == 0)
			{
				logInfo("Kicking slave for not authenticating after 5 seconds");
				connection.close();
			}
			else
			{
				setupPingTimer();
			}
		});
	}

	void gotRemoved() @trusted
	{
		if (info.pid)
		{
			// give 10s to reconnect
			sleep(10.seconds);
			foreach (slave; slaves)
				if (slave.info.pid == info.pid)
					return; // successful reconnect!

			relayMessage(info.pid, "close", Bson.emptyObject);
		}

		string apiId = info.apiId;
		if (!info.available && apiId.length)
		{
			string channel = "#mp_" ~ apiId;
			rootUser.joinChannel(channel);
			rootUser.sendMessage(channel, "Node managing this room has unexpectedly shutdown. This room will forcibly close in 10 minutes if the node doesn't reconnect.");
			rootUser.sendMessage(channel, "!mp clearhost");
			sleep(5.seconds);
			rootUser.sendMessage(channel, "!mp timer 600");
			sleep(10.minutes);
			rootUser.sendMessage(channel, "!mp close");
		}
	}
}

class AdminTCPConnection : TCPSlaveConnection
{
@safe:
	this(string host, ushort port, Duration timeout = 20.seconds)
	{
		super(host, port, timeout);
	}

	this(NetworkAddress addr, Duration timeout = 20.seconds)
	{
		super(addr, timeout);
	}

	/// Creates a new TCPSlaveConnection instance and takes control of the passed tcp connection.
	/// The receive loop needs to be started using receiveLoop(). It is run synchroniously.
	this(TCPConnection connection)
	{
		super(connection);
	}

	override void received(string type, Bson data)
	{
		if (type == RelayMessage.stringof)
		{
			auto msg = deserializeBson!RelayMessage(data);

			ManagerTCPSlaveConnection slave = getSlaveByPidOrNull(msg.pid);

			if (slave)
			{
				if (msg.t == QueueShutdown.stringof)
					slave.info.queuedShutdown = true;

				slave.sendPacket(msg.t, msg.d);
			}
			else
			{
				logError("Failed finding PID %s to relay %s message to", msg.pid, msg.t);
			}
		}
		else
		{
			logWarn("Unknown admin message type received: %s", type);
		}
	}

	void relay(int pid, string type, Bson data)
	{
		sendPacket(RelayMessage(pid, type, data));
	}
}
