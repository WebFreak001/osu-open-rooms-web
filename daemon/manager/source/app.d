module app;
@safe:

import bancho.irc;

import core.thread : Thread;

import std.algorithm;
import std.conv;
import std.functional;
import std.getopt;
import std.path;
import std.string;

import vibe.core.concurrency;
import vibe.core.core;
import vibe.core.file;
import vibe.core.log;
import vibe.core.net;
import vibe.core.sync;
import vibe.data.json;
import vibe.data.bson;

import orw.settings;
import orw.protocol.types;

int main(string[] args)
{
	import manager : config, connectBancho, initConfig;

	auto localConfig = readConfig();

	auto options = args.getopt("x|key", &localConfig.password, "h|host", &localConfig.hosts,
			"P|port", &localConfig.port, "a|admin-host", &localConfig.adminHosts, "q|admin-port",
			&localConfig.adminPort, "u|username", &localConfig.bancho.username, "p|password",
			&localConfig.bancho.password, std.getopt.config.passThrough);
	if (options.helpWanted)
	{
		(() @trusted => defaultGetoptPrinter("OsuOpenRoomsWeb slave manager & relay", options.options))();
		return 1;
	}

	(() @trusted => initConfig(localConfig))();

	Thread.getThis().name = "manager-main";

	if (!config.password.length && config.adminHosts.any!(a => !a.among!("127.0.0.1", "::1")))
		logWarn("Starting without admin password on public interface");

	connectBancho();

	TCPListener[] listeners;

	foreach (host; config.hosts)
		listeners ~= listenTCP(config.port, (() @trusted => toDelegate(&onConnection))(), host);

	foreach (host; config.adminHosts)
		listeners ~= listenTCP(config.adminPort, (() @trusted => toDelegate(&onAdmin))(), host);

	logInfo("Listening on %(%s, %)", listeners.map!"a.bindAddress");

	return runEventLoop();
}

void onConnection(TCPConnection conn) @safe nothrow
{
	import manager : ManagerTCPSlaveConnection, addSlave, removeSlave;

	try
	{
		logDebug("Incoming connection from %s", conn.peerAddress);

		auto slave = new ManagerTCPSlaveConnection(conn);
		scope (exit)
			slave.shutdown();

		addSlave(slave);
		scope (exit)
			removeSlave(slave);

		slave.autoKickTask();
		logDebug("Starting receive loop");
		slave.receiveLoop();
	}
	catch (Exception e)
	{
		logError("Failed processing connection from %s: %s", conn.peerAddress, e.msg);
	}
}

void onAdmin(TCPConnection conn) @safe nothrow
{
	import manager : AdminTCPConnection, addAdminRelay, removeAdminRelay;

	try
	{
		logDebug("Incoming admin connection from %s", conn.peerAddress);

		auto admin = new AdminTCPConnection(conn);

		addAdminRelay(admin);
		scope (exit)
			removeAdminRelay(admin);

		admin.receiveLoop();
	}
	catch (Exception e)
	{
		logError("Failed processing admin connection from %s: %s", conn.peerAddress, e.msg);
	}
}
