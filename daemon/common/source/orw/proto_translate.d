module orw.proto_translate;

import orw.protocol.types;

import std.traits;

import vibe.data.bson;

bool handlePacket(PacketDirection dir, T, Args...)(T handler, string type, Bson data, Args args)
		if (dir == PacketDirection.slaveToHost || dir == PacketDirection.hostToSlave)
{
	switch (type)
	{
		static foreach (typeStr; __traits(allMembers, orw.protocol.types))
		{
			static if (hasUDA!(__traits(getMember, orw.protocol.types, typeStr),
					dir) || hasUDA!(__traits(getMember, orw.protocol.types, typeStr),
					PacketDirection.bidirectional))
			{
	case __traits(getMember, orw.protocol.types, typeStr).stringof:
				handler.onPacket(deserializeBson!(__traits(getMember,
						orw.protocol.types, typeStr))(data), args);
				return true;
			}
		}
	default:
		return false;
	}
}
