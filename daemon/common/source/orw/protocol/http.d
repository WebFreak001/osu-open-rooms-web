module orw.protocol.http;

import orw.protocol.types;

import vibe.core.log;
import vibe.data.json;
import vibe.data.serialization;
import vibe.utils.dictionarylist;

import std.algorithm;
import std.array;
import std.ascii;
import std.conv;
import std.datetime.date;
import std.datetime.systime;
import std.datetime.timezone;
import std.exception;
import std.format;
import std.math;
import std.regex;
import std.string;
import std.traits;
import xmlcompat : encode;

private alias FormRequest = DictionaryList!(string, true, 16L, false);

class UserInputException : Exception
{
	string id;

	this(string msg, string id, string file = __FILE__, size_t line = __LINE__, Throwable parent = null) pure nothrow @nogc @safe
	{
		super(msg, file, line, parent);

		this.id = id;
	}
}

struct ModeSettings
{
	Json base = Json.emptyObject;
	ModeSetting[] settings;

	string renderHTML()
	{
		auto ret = appender!string;
		foreach (setting; settings)
			ret.put(setting.renderHTML(base));
		return ret.data;
	}

	UserInputException[] fromHTTP(FormRequest req)
	{
		auto ret = appender!(UserInputException[]);
		foreach (ref setting; settings)
		{
			try
			{
				setting.fromHTTP(req, base);
			}
			catch (UserInputException e)
			{
				ret.put(e);
			}
		}
		return ret.data;
	}
}

struct ModeSetting
{
	string[] path;
	string label;
	string description;
	SettingValue value;

	string type() const @property
	{
		return value.type;
	}

	string id() const @property
	{
		return value.type ~ "_" ~ path.join("__");
	}

	string renderHTML(Json options)
	{
		string id = this.id;
		string lbl = label.length ? format!`<label for="%s">%s</label>`(id, label.encode) : null;
		string desc = description.length ? "<p>" ~ description.encode ~ "</p>" : null;

		if (value.type == Checkbox.stringof)
			return format!`<div id="%s_label" class="mode-setting %s">%s%s%s</div>`(id,
					value.type, value.renderHTML(id, read(options)), lbl, desc);
		else
			return format!`<div id="%s_label" class="mode-setting %s">%s%s%s</div>`(id,
					value.type, lbl, value.renderHTML(id, read(options)), desc);
	}

	void fromHTTP(FormRequest req, ref Json bson)
	{
		string id = this.id;
		try
		{
			write(bson, value.fromHTTP(req, id));
		}
		catch (Exception e)
		{
			throw new UserInputException(e.msg, id, e.file, e.line, e);
		}
	}

	Json* access(return ref Json bson, bool create)
	{
		if (!path.length)
			return &bson;

		Json current = bson;
		if (current.type != Json.Type.object)
			throw new Exception("Can't access a non-object field");
		auto obj = current.get!(Json[string]);
		string part;

		foreach (sub; path[0 .. $ - 1])
		{
			part = sub;
			obj = current.get!(Json[string]);
			if (part in obj)
			{
				current = obj[part];
			}
			else
			{
				if (!create)
					return null;

				obj[part] = Json.emptyObject;
				current = obj[part];
			}

			if (current.type != Json.Type.object)
				throw new Exception("Can't traverse non-object bson in path " ~ path.join(
						".") ~ " for " ~ value.type ~ " setting of " ~ label);
		}

		obj = current.get!(Json[string]);
		if (auto ptr = path[$ - 1] in obj)
		{
			return ptr;
		}
		else
		{
			if (!create)
				return null;
			obj[path[$ - 1]] = Json.init;
			return path[$ - 1] in obj;
		}
	}

	void write(ref Json bson, Json value)
	{
		*access(bson, true) = value;
	}

	Json read(Json bson)
	{
		auto ret = access(bson, false);
		return ret ? *ret : Json.init;
	}
}

unittest
{
	Json test = Json([
			"foo": Json(4),
			"bar": Json(["x": Json("y"), "y": Json("f")])
			]);

	ModeSetting setting;
	setting.path = ["foo"];
	assert(setting.read(test).get!int == 4);
	setting.write(test, Json(5));
	assert(test["foo"].get!int == 5, test.toString);
	assert(setting.read(test).get!int == 5);

	setting.path = ["bar", "y"];
	assert(setting.read(test).get!string == "f");
	setting.write(test, Json("z"));
	assert(test["bar"]["y"].get!string == "z");
	assert(setting.read(test).get!string == "z");
}

struct Labeled(T)
{
	string label;
	T value;
}

/// Represents any text
struct Input
{
	enum Type
	{
		text,
		number,
		range,
		email,
		date,
		time,
		datetimeLocal
	}

	Type type = Type.text;
	string placeholder;
	int minLength, maxLength;
	string min, max;
	double step;
	string pattern;
	bool required;

	string renderHTML(string id, Json data)
	{
		auto attrs = appender!string;
		if (placeholder.length)
		{
			attrs.put(` placeholder="`);
			attrs.put(placeholder.encode);
			attrs.put('"');
		}
		if (minLength)
		{
			attrs.put(` minlength="`);
			attrs.put(minLength.to!string);
			attrs.put('"');
		}
		if (maxLength)
		{
			attrs.put(` maxlength="`);
			attrs.put(maxLength.to!string);
			attrs.put('"');
		}
		if (min.length)
		{
			attrs.put(` min="`);
			attrs.put(min.encode);
			attrs.put('"');
		}
		if (max.length)
		{
			attrs.put(` max="`);
			attrs.put(max.encode);
			attrs.put('"');
		}
		if (isFinite(step))
		{
			attrs.put(` step="`);
			attrs.put(step.to!string);
			attrs.put('"');
		}
		if (pattern)
		{
			attrs.put(` pattern="`);
			attrs.put(pattern.encode);
			attrs.put('"');
		}
		if (required)
			attrs.put(" required");

		if (data != Json.init)
		{
			attrs.put(` value="`);
			attrs.put(data.to!string.encode);
			attrs.put(`"`);
		}

		return format!`<input name="%1$s" id="%1$s" type="%2$s"%3$s/>`(id, type, attrs.data);
	}

	private T verifyNum(T)(T num)
	{
		static if (isFloatingPoint!T)
			enforce(isFinite(num), "Value is not finite");

		if (min.length)
			enforce(num >= min.to!double, "Value is smaller than the allowed minimum");
		if (max.length)
			enforce(num <= max.to!double, "Value is bigger than the allowed maximum");

		if (isFinite(step) && step != 0)
		{
			static if (isFloatingPoint!T)
			{
				const part = fmod(num, step);
				enforce(abs(part) <= 1e-6, "Value does not align with the allowed steps");
				// correct minor offsets
				num -= part;
			}
			else
			{
				enforce(num % cast(T) step == 0, "Value does not align with the allowed steps");
			}
		}

		return num;
	}

	Json fromHTTP(FormRequest req, string id)
	{
		auto data = req.get(id, "");
		if (required)
			enforce(data.length, "No value given for required field");

		if (pattern.length)
			enforce(matchFirst(data, pattern), "Value does not match the given pattern");

		final switch (type)
		{
		case Input.Type.number:
		case Input.Type.range:
			if (isFinite(step)
					&& step % 1 == 0)
				return Json(verifyNum(data.to!long));
			else
				return Json(verifyNum(data.to!double));
		case Input.Type.date:
			enforce(data.length == "YYYY-MM-DD".length,
					"Value must be in YYYY-MM-DD format.");
			Date.fromISOExtString(data);
			return Json(data);
		case Input.Type.datetimeLocal:
			enforce(data.length == "YYYY-MM-DDTHH:MM".length,
					"Value must be in YYYY-MM-DDTHH:MM format.");
			data ~= ":00Z";
			SysTime.fromISOExtString(data);
			return Json(data);
		case Input.Type.text:
		case Input.Type.email:
		case Input.Type.time:
			return Json(data);
		}
	}
}

/// Represents a single value selectable from a list of values.
struct Options
{
	/// Show a select input instead of radio buttons.
	bool select;
	/// Pair of options with labels which bson value to give as value.
	Labeled!Json[] options;
	/// Default text for select input.
	string placeholder;
	bool required;

	string renderHTML(string id, Json value)
	{
		auto ret = appender!string;
		immutable index = options.countUntil!(a => a.value == value);

		if (select)
		{
			ret.put(`<select id="`);
			ret.put(id);
			ret.put('"');
			if (required)
				ret.put(" required");
			ret.put(">");
			ret.put(`<option value=""`);
			if (index == -1)
				ret.put(" selected");
			if (required)
				ret.put(" disabled");
			ret.put(">");
			ret.put(placeholder.encode);
			ret.put(`</option>`);
			foreach (i, option; options)
			{
				string val = i.to!string(16);

				ret.put(`<option value="`);
				ret.put(val);
				ret.put(`"`);
				if (index == i)
					ret.put(" selected");
				ret.put(`>`);
				ret.put(option.label.encode);
				ret.put(`</option>`);
			}
			ret.put("</select>");
		}
		else
		{
			foreach (i, option; options)
			{
				string val = i.to!string(16);

				ret.put(`<div>`);
				ret.put(`<input type="radio" id="`);
				ret.put(id);
				ret.put("_");
				ret.put(val);
				ret.put(`" name="`);
				ret.put(id);
				ret.put(`" value="`);
				ret.put(val);
				ret.put(`"`);
				if (required)
					ret.put(" required");
				if (index == i)
					ret.put(" checked");
				ret.put(`>`);
				ret.put(`<label for="`);
				ret.put(id);
				ret.put("_");
				ret.put(val);
				ret.put(`">`);
				ret.put(option.label.encode);
				ret.put(`</label>`);
				ret.put(`</div>`);
			}
		}

		return ret.data;
	}

	Json fromHTTP(FormRequest req, string id)
	{
		auto data = req.get(id, "");
		enforce(!required || data.length, "A selection must be made for this field");
		enforce(data.all!(a => a.isHexDigit), "Internal error processing value format");
		enforce(data.length <= 8, "Value goes beyond acceptable scope");
		auto i = data.to!uint(16);
		enforce(i < options.length, "Value not in options");
		return options[i].value;
	}
}

/// Represents a list of values with individual labels for the user.
struct Checkboxes
{
	/// Options to show the user. Value order is guaranteed to be same as this array.
	Labeled!Json[] options;

	string renderHTML(string id, Json value)
	{
		auto ret = appender!string;

		Json[] values = value.type == Json.Type.array ? value.get!(Json[]) : null;

		foreach (i, option; options)
		{
			string val = i.to!string(16);

			ret.put(`<div>`);
			ret.put(`<input type="checkbox" id="`);
			ret.put(id);
			ret.put("_");
			ret.put(val);
			ret.put(`" name="`);
			ret.put(id);
			ret.put(`" value="`);
			ret.put(val);
			ret.put(`"`);
			if (values.canFind(option.value))
				ret.put(" checked");
			ret.put(`>`);
			ret.put(`<label for="`);
			ret.put(id);
			ret.put("_");
			ret.put(val);
			ret.put(`">`);
			ret.put(option.label.encode);
			ret.put(`</label>`);
			ret.put(`</div>`);
		}

		return ret.data;
	}

	Json fromHTTP(FormRequest req, string id)
	{
		auto data = req.getAll(id);
		uint[] indices;
		foreach (item; data)
		{
			enforce(item.all!(a => a.isHexDigit), "Internal error processing value format");
			enforce(item.length <= 8, "Value goes beyond acceptable scope");
			auto i = item.to!uint(16);
			enforce(i < options.length, "Value not in options");
			indices ~= i;
		}
		return Json(indices.sort!"a<b"
				.map!(a => options[a].value)
				.array);
	}
}

/// represents a BsonValueID for a map list.
struct MapList
{
	string renderHTML(string id, Json value)
	{
		throw new Exception(typeof(this)
				.stringof ~ ".renderHTML is not implemented, implement at caller-side");
	}

	Json fromHTTP(FormRequest req, string id)
	{
		auto data = req.get(id, "").replace("-", "").toLower;
		enforce(data.length, "A maplist must be picked for this field");
		enforce(data.length == 24 && data.all!(a => a.isHexDigit), "An invalid maplist ID was provided");
		BsonObjectID.fromString(data);
		return Json(data);
	}
}

/// Represents a boolean value.
struct Checkbox
{
	bool required;

	string renderHTML(string id, Json value)
	{
		auto ret = appender!string;
		ret.put(`<input type="checkbox" id="`);
		ret.put(id);
		ret.put(`" name="`);
		ret.put(id);
		ret.put(`" value="1"`);
		if (value.type == Json.Type.bool_ && value.get!bool)
			ret.put(" checked");
		ret.put(`>`);

		return ret.data;
	}

	Json fromHTTP(FormRequest req, string id)
	{
		auto data = req.get(id, "0");
		return Json(data == "1");
	}
}

struct SettingValue
{
	import std.variant : Algebraic;

	alias Types = typeof(value).AllowedTypes;

	@ignore Algebraic!(Input, Options, Checkboxes, Checkbox, MapList) value;

	this(T)(T value)
	{
		this.value = Algebraic!Types(value);
	}

	string renderHTML(string id, Json val)
	{
		if (!value.hasValue)
			return null;

		static foreach (T; Types)
			if (auto v = value.peek!T)
				return v.renderHTML(id, val);

		assert(false);
	}

	Json fromHTTP(FormRequest req, string id)
	{
		if (!value.hasValue)
			return Json.init;

		static foreach (T; Types)
			if (auto v = value.peek!T)
				return v.fromHTTP(req, id);

		assert(false);
	}

	Json toRepresentation() const @trusted
	{
		if (!value.hasValue)
			return Json.init;

		static foreach (T; Types)
			if (auto v = value.peek!T)
				return Json(["t": Json(T.stringof), "v": serializeToJson(*v)]);

		assert(false);
	}

	string type() const @property
	{
		if (!value.hasValue)
			return null;

		static foreach (T; Types)
			if (auto v = value.peek!T)
				return T.stringof;

		assert(false);
	}

	static SettingValue fromRepresentation(Json value) @trusted
	{
		if (value == Json.init || value.type != Json.Type.object)
			throw new Exception("Invalid SettingValue data, must be an object");

		auto obj = value.get!(Json[string]);
		if (obj.empty)
			throw new Exception("Invalid SettingValue data, passed in an empty object");

		auto typePtr = "t" in obj;
		auto valuePtr = "v" in obj;

		if (!typePtr)
			throw new Exception("Invalid SettingValue data, missing type string 't' key");
		if (!valuePtr)
			throw new Exception("Invalid SettingValue data, missing value 'v' key");
		if (typePtr.type != Json.Type.string)
			throw new Exception("Invalid SettingValue data, value of t must be string");

		string type = typePtr.get!string;

		static foreach (T; Types)
			if (type == T.stringof)
				return SettingValue(Algebraic!Types(deserializeJson!T(*valuePtr)));

		throw new Exception("Unsupported type " ~ type ~ " for " ~ typeof(this).stringof);
	}
}

static assert(isCustomSerializable!SettingValue);
