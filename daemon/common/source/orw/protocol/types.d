module orw.protocol.types;

import bancho.irc;

import core.time;

public import vibe.data.bson;
public import orw.protocol.http;
public import orw.protocol.serialize : wrapTask;

enum PacketDirection
{
	bidirectional,
	slaveToHost,
	hostToSlave
}

struct Optional(T)
{
	bool set;
	T value;

	this(T value)
	{
		set = true;
		this.value = value;
	}

	alias get this;

	T get()
	{
		if (set)
			return value;
		else
			return T.init;
	}

	void opAssign(T value)
	{
		set = true;
		this.value = value;
	}

	Bson toRepresentation() const
	{
		if (set)
			return serializeToBson(value);
		else
			return Bson(null);
	}

	static Optional!T fromRepresentation(Bson value)
	{
		if (value.isNull)
			return Optional!T.init;
		else
			return Optional!T(deserializeBson!T(value));
	}
}

enum int DMManagerPid = -10000;

struct RelayMessage
{
	int pid;
	string t;
	Bson d;
}

struct ModeID
{
	string id;
	long version_;
	string name;
}

struct SlaveInfo
{
	this(OpRegister register) @safe
	{
		this.name = register.name;
		this.pid = register.pid;
		this.protoVersion = register.protoVersion;
		this.available = !register.alreadyOccupied;
		this.buildDate = register.buildDate;
		this.availableModes = register.availableModes;
	}

	string name;
	int pid;
	int protoVersion = OpRegister.init.protoVersion;
	bool available = !OpRegister.init.alreadyOccupied;
	string buildDate;
	ModeID[] availableModes;

	bool manageDMs;
	string gameId;
	string apiId;
	bool queuedShutdown;
}

struct SerializableDuration
{
@safe:
	Duration value;

	this(Duration v)
	{
		value = v;
	}

	alias value this;

	string toString() const
	{
		return value.serialize;
	}

	static SerializableDuration fromString(string s)
	{
		return SerializableDuration(deserializeDuration(s));
	}

	static Bson toBson(SerializableDuration d)
	{
		return Bson(d.toString);
	}

	static SerializableDuration fromBson(Bson v)
	{
		return SerializableDuration.fromString(v.get!string);
	}
}

static assert(isStringSerializable!SerializableDuration);

string serialize(Duration d) @safe
{
	import std.conv : to;

	if (d == Duration.zero)
		return "0 s";

	const parts = d.split;

	if (parts.hnsecs != 0)
		return d.total!"hnsecs"
			.to!string ~ " hns";
	else if (parts.usecs != 0)
		return d.total!"usecs"
			.to!string ~ " us";
	else if (parts.msecs != 0)
		return d.total!"msecs"
			.to!string ~ " ms";
	else if (parts.seconds != 0)
		return d.total!"seconds"
			.to!string ~ " s";
	else
		return d.total!"minutes"
			.to!string ~ " m";
}

Duration deserializeDuration(string s) @safe
{
	import std.conv : to;
	import std.string : endsWith, strip;

	s = s.strip;

	long hns;
	if (s.endsWith(" hns"))
		hns += s[0 .. $ - 3].strip.to!long;
	else if (s.endsWith(" us"))
		hns += cast(long)(s[0 .. $ - 2].strip.to!double * 10);
	else if (s.endsWith(" ms"))
		hns += cast(long)(s[0 .. $ - 2].strip.to!double * 10_000);
	else if (s.endsWith(" s"))
		hns += cast(long)(s[0 .. $ - 1].strip.to!double * 10_000_000);
	else if (s.endsWith(" secs"))
		hns += cast(long)(s[0 .. $ - 4].strip.to!double * 10_000_000);
	else if (s.endsWith(" seconds"))
		hns += cast(long)(s[0 .. $ - 7].strip.to!double * 10_000_000);
	else if (s.endsWith(" m"))
		hns += cast(long)(s[0 .. $ - 1].strip.to!double * 600_000_000);
	else if (s.endsWith(" mins"))
		hns += cast(long)(s[0 .. $ - 4].strip.to!double * 600_000_000);
	else if (s.endsWith(" minutes"))
		hns += cast(long)(s[0 .. $ - 7].strip.to!double * 600_000_000);
	else
		throw new Exception("Duration doesn't end with hns, us, ms, s or m");
	return hns.hnsecs;
}

unittest
{
	void test(string t)()
	{
		assert(mixin("-1000." ~ t).serialize.deserializeDuration == mixin("-1000." ~ t));
		assert(mixin("-100." ~ t).serialize.deserializeDuration == mixin("-100." ~ t));
		assert(mixin("-10." ~ t).serialize.deserializeDuration == mixin("-10." ~ t));
		assert(mixin("-2." ~ t).serialize.deserializeDuration == mixin("-2." ~ t));
		assert(mixin("-1." ~ t).serialize.deserializeDuration == mixin("-1." ~ t));
		assert(mixin("1." ~ t).serialize.deserializeDuration == mixin("1." ~ t));
		assert(mixin("2." ~ t).serialize.deserializeDuration == mixin("2." ~ t));
		assert(mixin("10." ~ t).serialize.deserializeDuration == mixin("10." ~ t));
		assert(mixin("100." ~ t).serialize.deserializeDuration == mixin("100." ~ t));
		assert(mixin("1000." ~ t).serialize.deserializeDuration == mixin("1000." ~ t));
	}

	assert(Duration.zero.serialize.deserializeDuration == Duration.zero);
	test!"hnsecs";
	test!"usecs";
	test!"msecs";
	test!"seconds";
	test!"minutes";
	test!"hours";
}

@(PacketDirection.slaveToHost)
struct OpRegister
{
	this(string password, SlaveInfo info) @safe
	{
		this.password = password;
		this.name = info.name;
		this.pid = info.pid;
		this.protoVersion = info.protoVersion;
		this.alreadyOccupied = !info.available;
		this.buildDate = buildDate;
		this.availableModes = info.availableModes;
	}

	string password;
	string name;
	int pid;
	int protoVersion = 1;
	bool alreadyOccupied;
	string buildDate = __TIMESTAMP__;
	ModeID[] availableModes;
}

@(PacketDirection.hostToSlave)
struct OpManageDMs
{
}

struct RoomSettings
{
	@optional Optional!string password;
	int slots = 8;
	bool locked;
	TeamMode teammode;
	ScoreMode scoremode;
	GameMode gamemode;
	Mod[] mods;
	string[] refs;
	string[] invites;

	SerializableDuration ttl;
	string[] owners;

	OpRoomUpdateDurationsRequest durations;
}

@(PacketDirection.hostToSlave)
struct OpRoomUpdateDurationsRequest
{
	SerializableDuration startGameDuration = 3.minutes;
	SerializableDuration allReadyStartDuration = 5.seconds;
	SerializableDuration manualStartDuration = 15.seconds;
@optional:
	SerializableDuration retryGameDuration = 30.seconds;
	SerializableDuration retryEvaluationDuration = 20.seconds;
}

@(PacketDirection.hostToSlave)
struct OpModeSettingsRequest
{
	string mode;
}

@(PacketDirection.slaveToHost)
struct OpModeSettingsResponse
{
	ModeID id;
	ModeSettings settings;

	bool valid() @property
	{
		return !!id.id.length;
	}
}

@(PacketDirection.hostToSlave)
struct OpRoomModeSettings
{
	string mode;
	Json config;
}

// @(PacketDirection.hostToSlave)
// struct OpRoomModeMiniHostRotate
// {
// 	enum CycleMode
// 	{
// 		fair,
// 		downwards,
// 		upwards,
// 		random,
// 		winner
// 	}

// 	/// Number of maps to look back in playing history to deny. (Max 100)
// 	int recentlyPlayedLength = 5;
// 	SerializableDuration selectWarningTimeout = 90.seconds;
// 	SerializableDuration selectIdleChangeTimeout = 30.seconds;
// 	CycleMode mode;
// }

enum ApprovalFlags
{
	graveyard = 1 << 0,
	wip = 1 << 1,
	pending = 1 << 2,
	ranked = 1 << 3,
	approved = 1 << 4,
	qualified = 1 << 5,
	loved = 1 << 6,
	all = graveyard | wip | pending | ranked | approved | qualified | loved,
}

enum GenreFlags
{
	unknown = 1 << 0,
	videoGame = 1 << 1,
	anime = 1 << 2,
	rock = 1 << 3,
	pop = 1 << 4,
	other = 1 << 5,
	novelty = 1 << 6,
	hipHop = 1 << 7,
	electronic = 1 << 8,
	all = unknown | videoGame | anime | rock | pop | other | novelty | hipHop | electronic
}

enum LanguageFlags
{
	unknown = 1 << 0,
	other = 1 << 1,
	english = 1 << 2,
	japanese = 1 << 3,
	chinese = 1 << 4,
	instrumental = 1 << 5,
	korean = 1 << 6,
	french = 1 << 7,
	german = 1 << 8,
	swedish = 1 << 9,
	spanish = 1 << 10,
	italian = 1 << 11,
	all = unknown | other | english | japanese | chinese | instrumental | korean
		| french | german | swedish | spanish | italian
}

enum ModeFlags
{
	osu = 1 << 0,
	taiko = 1 << 1,
	ctb = 1 << 2,
	mania = 1 << 3,
	all = osu | taiko | ctb | mania
}

@(PacketDirection.hostToSlave)
struct OpConfigureSkip
{
	double multiplier = 0.5;
	ApprovalFlags preferredApproval = ApprovalFlags.all;
	GenreFlags preferredGenre = GenreFlags.all;
	LanguageFlags preferredLanguage = LanguageFlags.all;
	ModeFlags preferredMode = ModeFlags.all;
	SerializableDuration maxSongLength = 10.minutes;
	SerializableDuration minSongLength = 1.minutes;
}

@(PacketDirection.hostToSlave)
struct OpRoomSubmitRequest
{
	string name;
	RoomSettings settings;
}

@(PacketDirection.slaveToHost)
struct OpRoomSubmitResponse
{
	/// True iff the room has been opened.
	bool created;
	/// Human readable error message in case of failed to create.
	string error;
	/// Room ID for ingame usage or join links in format osu://mp/<gameId>
	string gameId;
	/// Room ID as seen in the website URL or in API requests.
	string apiId;
	/// True if the node is in a not-available state already or after the command.
	bool occupied;
}

@(PacketDirection.hostToSlave)
struct OpRoomSubmitAccept
{
	/// Used to verify that the OpRoomSubmitResponse successfully got processed on the other side.
	string gameId;
}

@(PacketDirection.hostToSlave)
struct OpClose
{
	string reason;
	bool finishMap = true;
}

@(PacketDirection.slaveToHost)
struct OpClosed
{
	string reason;
}

@(PacketDirection.hostToSlave)
struct OpConfigMessage
{
	string message;
}

@(PacketDirection.hostToSlave)
struct OpSendPrivateMessage
{
	string user;
	string message;
}

@(PacketDirection.slaveToHost)
struct OpSettings
{
	OsuRoom.Settings settings;
}

@(PacketDirection.slaveToHost)
struct OpPlayerChange
{
	string name;
	@optional Optional!bool host;
	@optional Optional!Team team;
	@optional Optional!int slot;
	@optional Optional!bool leave;
}

@(PacketDirection.slaveToHost)
struct OpHostCleared
{
}

@(PacketDirection.slaveToHost)
struct OpPlayerMessage
{
	string room;
	string name;
	string message;
}

@(PacketDirection.hostToSlave)
struct OpSimulatePlayerMessage
{
	string room;
	string name;
	string message;
}

@(PacketDirection.slaveToHost)
struct OpMatchStart
{
}

@(PacketDirection.slaveToHost)
struct OpMatchFinish
{
}

@(PacketDirection.slaveToHost)
struct OpBeatmapPending
{
}

@(PacketDirection.slaveToHost)
struct OpBeatmapChanged
{
	BeatmapInfo map;
}

@(PacketDirection.hostToSlave)
struct QueueShutdown
{
}
