module app;
@safe:

import bancho.irc;

import core.thread : Thread;
import core.time;

import vibe.core.core;
import vibe.core.log;
import vibe.core.net;

import orw.settings;
import orw.protocol.types;

import osu.api.config;

import std.algorithm;
import std.getopt;
import std.process : thisProcessID;

import db : registerDB;

__gshared MonoTime startupTime;

Duration getUptime() @trusted
{
	return MonoTime.currTime - startupTime;
}

int main(string[] args)
{
	import slave : config, state, connectBancho, rootUser, ClientTCPSlaveConnection;

	version (Posix)
	{
		import core.sys.posix.signal : signal, SIGPIPE, SIG_IGN;

		(() @trusted => signal(SIGPIPE, SIG_IGN))();
	}

	auto localConfig = readConfig();

	auto options = args.getopt("x|key", &localConfig.password, "h|host", &localConfig.hosts,
			"P|port", &localConfig.port, "a|admin-host", &localConfig.adminHosts, "q|admin-port",
			&localConfig.adminPort, "u|username", &localConfig.bancho.username, "p|password",
			&localConfig.bancho.password, std.getopt.config.passThrough);

	(() @trusted => cast()config = localConfig)();

	if (options.helpWanted)
	{
		(() @trusted => defaultGetoptPrinter("OsuOpenRoomsWeb slave", options.options))();
		return 1;
	}

	Thread.getThis().name = "slave-main";

	runTask({
		MonoTime last = MonoTime.currTime;
		while (true)
		{
			sleep(500.msecs);
			auto now = MonoTime.currTime;
			if (now - last > 505.msecs)
				logWarn("Running behind! A step of half a second just took %s instead of 500 msecs. Is the system overloaded?",
					now - last);
			last = now;
		}
	});

	(() @trusted => cast()startupTime = MonoTime.currTime)();

	registerDB("mongodb://127.0.0.1");

	state.info.pid = thisProcessID;
	state.info.name = config.bancho.username;

	(() @trusted => cast()banchoApiBase = config.bancho.apiBase)();
	(() @trusted => cast()banchoApiKey = config.bancho.apiKey)();

	connectBancho();

	auto delay = runTask({
		try
		{
			sleep(10.seconds);
		}
		catch (InterruptException e)
		{
		}
	});

	void abortDelay() nothrow
	{
		rootUser.onAuthenticated = rootUser.onAuthenticated.remove!(a => a == &abortDelay);
		delay.interrupt();
	}

	rootUser.onAuthenticated ~= &abortDelay;

	delay.join();

	if (!rootUser.client.connected)
		throw new Exception("Bancho IRC isn't connected");

	// sanity delay to make bancho connect fully
	sleep(100.msecs);

	size_t hostIndex = 0;
	while (!state.shutdownQueued)
	{
		TCPConnection conn;
		try
		{
			logDiagnostic("Connecting to %s:%s", config.hosts[hostIndex], config.port);
			conn = connectTCP(config.hosts[hostIndex], config.port, null, 0, 60.seconds);
		}
		catch (Exception e)
		{
			logError("Failed to connect to host %s:%s", config.hosts[hostIndex], config.port);
			hostIndex = (hostIndex + 1) % config.hosts.length;
			if (hostIndex == 0)
				sleep(2.seconds);
			continue;
		}
		scope (exit)
			conn.close();
		scope (exit)
			state.onDisconnect();

		auto host = new ClientTCPSlaveConnection(conn);
		state.connection = host;
		scope (exit)
			state.connection = null;
		runTask({
			logDiagnostic("Registering on master");
			host.register(config.password);
			host.pingTask();
		});
		logDebug("Starting receive loop");
		host.receiveLoop();
		host.close();
		if (!state.shutdownQueued)
			logError("Got disconnected from manager, reconnecting...");
	}

	return 0;
}
