module hostrotate.mode;
@safe:

import core.time;

import std.meta;

import orw.protocol.types;

import vibe.core.sync;

public import hostrotate.utils;

import bancho.irc;

mixin template NothrowRoomHandler()
{
	import std.traits : Parameters, ReturnType;

	ReturnType!fn nothrowOrEndRoom(alias fn)(Parameters!fn params) nothrow
	{
		import vibe.core.core : sleep;
		import vibe.core.log : logException;
		import std.functional : forward;
		import slave : state;

		try
		{
			return fn(forward!params);
		}
		catch (Exception e)
		{
			logException(e, "exception in nothrow callback " ~ __traits(identifier, fn));
			if (context.room)
				context.room.trySendMessage("A fatal exception ocurred. Room will be closed");

			try
			{
				sleep(10.seconds);
			}
			catch (Exception)
			{
			}
			state.closeRoom("exception in nothrow callback " ~ __traits(identifier, fn));

			static if (!is(ReturnType!fn == void))
				return ReturnType!fn.init;
		}
	}
}

abstract class HostRotateMode
{
	bool abortNextSwitch;
	bool allowSwitching;
	InterruptibleTaskCondition switchCondition;
	RoomContext context;

	abstract ModeID id() @property;
	abstract ModeSettings settings() @property;
	abstract void load(RoomContext context, Json args);
	abstract void unload();

	OsuRoom room()
	{
		if (!context.room)
			throw new Exception("Room no longer exists");
		return context.room;
	}

	bool readyForSwitch()
	{
		allowSwitching = true;
		if (!abortNextSwitch)
			return false;

		auto m = switchCondition.mutex;
		m.lock();
		scope (exit)
			m.unlock();
		if (!abortNextSwitch)
			return true;
		abortNextSwitch = false;
		switchCondition.notifyAll();
		return true;
	}

	void unreadyForSwitch()
	{
		allowSwitching = false;
	}

	bool waitForSwitch(Duration timeout = 2.hours)
	{
		if (allowSwitching)
			return true;

		if (abortNextSwitch)
		{
			auto m = switchCondition.mutex;
			m.lock();
			scope (exit)
				m.unlock();

			return switchCondition.wait(timeout);
		}
		else
		{
			auto m = new InterruptibleTaskMutex();
			switchCondition = new InterruptibleTaskCondition(cast(Lockable) m);
			abortNextSwitch = true;

			m.lock();
			scope (exit)
				m.unlock();
			return switchCondition.wait(timeout);
		}
	}

	/// Returns: true if message was processed as relevant.
	abstract bool onMessage(string room, string sender, string message, bool direct);

	abstract void skipMap();
}

string getRotateModeNames(alias module_)()
{
	string ret;
	static foreach (member; __traits(allMembers, module_))
		static if (is(__traits(getMember, module_, member) == class)
				&& is(__traits(getMember, module_, member) : HostRotateMode)
				&& !is(__traits(getMember, module_, member) == HostRotateMode))
			ret ~= member ~ ", ";
	return ret;
}

ModeID[] getModeIDs(Modes...)()
{
	ModeID[] ret;
	static foreach (Mode; Modes)
		ret ~= Mode._make().id;
	return ret;
}

template getRotateModes(alias module_)
{
	mixin("alias getRotateModes = AliasSeq!(" ~ getRotateModeNames!module_ ~ ");");
}

mixin template ModeSingleton()
{
	private static typeof(this) _instance;
	static typeof(this) getInstance()
	{
		if (!_instance)
			return _instance = new typeof(this)();

		return _instance;
	}

	enum info = _make.id;

	static typeof(this) _make()
	{
		return new typeof(this)();
	}

	private this()
	{
	}
}

public
{
	import hostrotate.mode.playlist;
}

alias AvailableModes = AliasSeq!(getRotateModes!(hostrotate.mode.playlist));
enum AvailableModeIDs = getModeIDs!AvailableModes;

pragma(msg, "Map Modes: ", AvailableModeIDs);
