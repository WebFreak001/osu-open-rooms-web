export type KnobAxis = "x" | "y" | "both";

export class CurveSlider extends HTMLElement {
	css: HTMLStyleElement;
	container: HTMLElement;
	bar: HTMLElement;
	knob: SliderKnob;
	ticks: HTMLElement[];

	constructor() {
		// Always call super first in constructor
		super();

		var shadow = this.attachShadow({ mode: "open" });

		this.css = document.createElement("style");

		this.css.textContent = `
			:host {
				position: relative;
				display: inline-block;
				max-width: 100%;
				width: 300px;
				height: 40px;
				padding: 10px;
				box-sizing: border-box;
			}

			:focus-within .bar {
				box-shadow: 0 0 2px #ffcc22;
			}

			slider-knob:focus {
				--knob-box-shadow: 0 0 4px 1px #ffcc22;
				--value-opacity: 1;
				z-index: 4;
			}

			.container {
				position: relative;
				width: 100%;
				height: 100%;
			}

			.bar, .tick, slider-knob {
				position: absolute;
				top: 0;
				bottom: 0;
				margin: auto;
			}

			slider-knob {
				z-index: 3;
				left: calc(var(--normalized-value) * 100%);
			}

			.bar {
				border-radius: var(--bar-border-radius, 0);
				border: var(--bar-border, none);
				background: var(--bar-color, var(--slider-color, white));
				height: var(--bar-height, 1px);
				left: 0;
				right: 0;
				z-index: 1;
			}

			.tick {
				pointer-events: none;
				background: var(--tick-color, var(--slider-color, white));
				opacity: var(--tick-opacity, 1);
				width: var(--tick-thickness, 1px);
				height: var(--tick-size, 5px);
				z-index: 2;
				left: calc(var(--tick-value) * 100%);
			}
		`;

		// attach the created elements to the shadow dom

		shadow.appendChild(this.css);
		this.render(shadow);
	}

	attributeChangedCallback(name: string, oldValue: string, newValue: string) {
		switch (name) {
			case "value":
			case "step":
				this.knob.valueNum = parseFloat(newValue);
				this.knob.value = this.formatValue(parseFloat(newValue));
				this.recompute();
				break;
			case "min":
			case "max":
			case "slope":
				this.recompute();
			/* falls through */
			case "tick-count":
				this.regenerateTicks();
				break;
			case "max-infinity":
			case "min-infinity":
				this.knob.value = this.formatValue(this.knob.valueNum);
				break;
			default:
				console.warn("unknown property change", name, oldValue, newValue);
				break;
		}
	}

	render(shadow: ShadowRoot): void {
		this.container = document.createElement("div");
		this.container.addEventListener("pointerdown", e => {
			e.preventDefault();
			this.handleBarDrag(this.container, e);
		});
		this.container.className = "container";
		this.container.appendChild(this.bar = this.renderBar());
		this.container.appendChild(this.knob = this.renderKnob());
		this.regenerateTicks();
		shadow.appendChild(this.container);
	}

	renderBar(): HTMLElement {
		let bar = document.createElement("div");
		bar.className = "bar";
		return bar;
	}

	regenerateTicks(): void {
		if (this.ticks)
			this.ticks.forEach(tick => {
				this.container.removeChild(tick);
			});
		this.ticks = this.renderTicks();
		this.ticks.forEach(tick => {
			this.container.appendChild(tick);
		});
		this.updateTicks();
	}

	updateTicks(): void {
		this.ticks.forEach(tick => {
			if (this.inSlider(parseFloat(tick.getAttribute("data-value")))) {
				if (!tick.classList.contains("between"))
					tick.classList.add("between");
			}
			else if (tick.classList.contains("between"))
				tick.classList.remove("between");
		});
	}

	/**
	 * checks if a given normalized value is in the user selected value range of this slider.
	 * @param v a normalized value in range 0-1
	 */
	inSlider(v: number): boolean {
		return v < parseFloat(this.style.getPropertyValue("--normalized-value"));
	}

	renderTicks(): HTMLElement[] {
		let ret: HTMLElement[] = [];
		const min = this.min;
		const max = this.max;
		const count = this.tickCount;
		for (let i = 0; i < count; i++) {
			const t = min + (max - min) / count * i;
			let tick = document.createElement("div");
			tick.className = "tick";
			const v = this.untransform(this.snapClamp(t));
			tick.setAttribute("data-value", v.toString());
			tick.style.setProperty("--tick-value", v.toString());
			ret.push(tick);
		}
		return ret;
	}

	renderKnob(value: number = this.value, index: number = 1): SliderKnob {
		let knob = <SliderKnob>document.createElement("slider-knob");
		const ev = this.handleKnobDrag.bind(this, knob);
		knob.addEventListener("pointerdown", e => {
			e.preventDefault();
			knob.setPointerCapture(e.pointerId);
			knob.addEventListener("pointermove", ev);
		});
		let cancel = e => {
			e.preventDefault();
			knob.releasePointerCapture(e.pointerId);
			knob.removeEventListener("pointermove", ev);
			this.finishKnobDrag(knob, knob.valueNum);
		};
		knob.addEventListener("pointercancel", cancel);
		knob.addEventListener("pointerup", cancel);

		knob.addEventListener("keydown", e => {
			if (e.altKey || (e.ctrlKey && e.shiftKey))
				return;

			if (e.key == "ArrowLeft") {
				e.preventDefault();
				this.nudgeKnob(knob, (e.ctrlKey ? 10 : 1) * (e.shiftKey ? 0.1 : 1) * -1);
			}
			else if (e.key == "ArrowRight") {
				e.preventDefault();
				this.nudgeKnob(knob, (e.ctrlKey ? 10 : 1) * (e.shiftKey ? 0.1 : 1) * 1);
			}
		});
		// for some reason you can't directly assign value here (it breaks future assignments, only in the single slider)
		knob.setAttribute("value", this.formatValue(value));
		knob.setAttribute("tabindex", index.toString());
		return knob;
	}

	handleKnobDrag(knob: SliderKnob, e: PointerEvent): void {
		const rect = this.getBoundingClientRect();
		const min = this.min;
		const max = this.max;
		let v = (e.clientX - rect.left) / rect.width;
		if (v < 0) v = 0;
		if (v > 1) v = 1;
		let vt = this.snapClamp(this.transform(v));
		v = this.untransform(vt);
		knob.valueNum = vt;
		knob.value = this.formatValue(vt);
		this.style.setProperty("--input-value", vt.toString());
		this.style.setProperty("--normalized-value", v.toString());

		this.updateTicks();
	}

	handleBarDrag(bar: HTMLElement, e: PointerEvent): void {
		this.startKnobDrag(this.knob, e.pointerId);
	}

	startKnobDrag(knob: SliderKnob, pointer: number): void {
		const ev = this.handleKnobDrag.bind(this, knob);
		let cancel = e => {
			e.preventDefault();
			knob.releasePointerCapture(e.pointerId);
			knob.removeEventListener("pointermove", ev);
			this.finishKnobDrag(knob, knob.valueNum);
		};
		knob.addEventListener("pointercancel", cancel);
		knob.addEventListener("pointerup", cancel);

		knob.setPointerCapture(pointer);
		knob.addEventListener("pointermove", ev);

		knob.focus();
	}

	finishKnobDrag(knob: SliderKnob, value: number): void {
		if (value === undefined || value === null)
			return;

		this.value = value;
	}

	calculateNudge(start: number, value: number): number {
		const step = this.step;
		if (step > 0) {
			const a = Math.abs(value);
			if (a < 1)
				return Math.sign(value) * step;
			else
				return Math.round(value / step) * step;
		} else {
			const offset = value / 100;
			const dt = this.max - this.min;
			if (dt > 1000000)
				return offset * 1000000;
			else if (dt > 10000)
				return offset * 10000;
			else if (dt > 100)
				return offset * 100;

			if (this.hasNegative) {
				return this.slope * 2 * offset;
			} else {
				return (this.slope - this.min) * 2 * offset;
			}
		}
	}

	nudgeKnob(knob: SliderKnob, value: number): void {
		this.value += this.calculateNudge(this.value, value);
	}

	static get observedAttributes() { return ["min", "max", "slope", "step", "value", "tick-count", "max-infinity", "min-infinity"]; }

	recompute(): void {
		const snapped = this.snapClamp(this.value);
		if (this.value != snapped)
			this.value = snapped;

		this.style.setProperty("--input-value", this.value.toString());
		this.style.setProperty("--normalized-value", this.untransform(this.value).toString());
		this.updateTicks();
	}

	/**
	 * Converts a linear value into the slider value space.
	 * @param x a linear value from 0-1
	 * @returns a slider space value from min-max
	 */
	transform(x: number): number {
		if (this.hasNegative) {
			if (x < 0.5) {
				x = 1 - x * 2;
				const factor = this.negFactor;
				return -((factor * this.slope * x) / (1 - factor * x));
			} else {
				x = (x - 0.5) * 2;
				const factor = this.posFactor;
				return (factor * this.slope * x) / (1 - factor * x);
			}
		} else {
			const min = this.min;
			const factor = this.factor;
			return ((factor * (this.slope - min) * x) / (1 - factor * x)) + min;
		}
	}

	/**
	 * Converts a slider space value into a linear value.
	 * @param x a slider space value from min-max
	 * @returns a linear value from 0-1
	 */
	untransform(x: number): number {
		if (this.hasNegative) {
			if (x < 0) {
				x = -x;
				return 0.5 - (x / (this.negFactor * (this.slope + x))) * 0.5;
			} else {
				return (x / (this.posFactor * (this.slope + x))) * 0.5 + 0.5;
			}
		} else {
			const min = this.min;
			x -= min;
			return x / (this.factor * ((this.slope - min) + x));
		}
	}

	formatValue(v: number): string {
		if (v + 0.000001 >= this.max && this.maxInfinity)
			return "∞";
		else if (v - 0.000001 <= this.min && this.minInfinity)
			return "-∞";

		const a = Math.abs(v);
		if (a > 100000)
			return Math.round(v).toString();
		else if (a > 1000)
			return (Math.round(v * 10) / 10).toString();
		else if (a > 100)
			return (Math.round(v * 100) / 100).toString();
		else if (a > 1)
			return (Math.round(v * 1000) / 1000).toString();
		else
			return (Math.round(v * 10000) / 10000).toString();
	}

	snapClamp(v: number): number {
		const step = this.step;
		if (step > 0 && Math.round((v % step) * 10000) / 10000 != 0)
			v = Math.round(v / step) * step;
		if (v > this.max)
			v = this.max;
		else if (v < this.min)
			v = this.min;
		return v;
	}

	get factor(): number {
		return (this.max - this.min) / (this.max + this.slope - 2 * this.min);
	}

	get negFactor(): number {
		return -this.min / (-this.min + this.slope);
	}

	get posFactor(): number {
		return this.max / (this.max + this.slope);
	}

	get hasNegative(): boolean { return this.min < 0; }

	get min(): number { return parseFloat(this.getAttribute("min") || "0"); }
	set min(min: number) { this.setAttribute("min", min.toString()); }

	get max(): number { return parseFloat(this.getAttribute("max") || "1"); }
	set max(max: number) { this.setAttribute("max", max.toString()); }

	get slope(): number { return parseFloat(this.getAttribute("slope") || "0.5"); }
	set slope(slope: number) { this.setAttribute("slope", slope.toString()); }

	get step(): number { return parseFloat(this.getAttribute("step") || "0"); }
	set step(step: number) { this.setAttribute("step", step.toString()); }

	get value(): number { return parseFloat(this.getAttribute("value") || "0"); }
	set value(value: number) { this.setAttribute("value", value.toString()); }

	get tickCount(): number { return parseInt(this.getAttribute("tick-count") || "12"); }
	set tickCount(tickCount: number) { this.setAttribute("tick-count", tickCount.toString()); }

	get minInfinity(): boolean { return this.getAttribute("min-infinity") !== null; }
	set minInfinity(minInfinity: boolean) { if (minInfinity) this.setAttribute("min-infinity", "min-infinity"); else this.removeAttribute("min-infinity") }

	get maxInfinity(): boolean { return this.getAttribute("max-infinity") !== null; }
	set maxInfinity(maxInfinity: boolean) { if (maxInfinity) this.setAttribute("max-infinity", "max-infinity"); else this.removeAttribute("max-infinity") }
}
window.customElements.define("curve-slider", CurveSlider);

export class SliderKnob extends HTMLElement {
	css: HTMLStyleElement;
	knob: HTMLElement;
	valueElem: HTMLElement;
	_valueNum: number;

	constructor() {
		// Always call super first in constructor
		super();

		var shadow = this.attachShadow({ mode: "open" });

		this.css = document.createElement("style");

		this.css.textContent = `
			:host {
				width: 1px;
				height: 1px;
				outline: none;
			}
			.knob {
				display: inline-block;
				position: relative;
				width: var(--knob-width, var(--diameter, 16px));
				height: var(--knob-height, var(--diameter, 16px));
				border-top-left-radius: var(--roundness-top-left, var(--roundness, 100%));
				border-bottom-left-radius: var(--roundness-bottom-left, var(--roundness, 100%));
				border-top-right-radius: var(--roundness-top-right, var(--roundness, 100%));
				border-bottom-right-radius: var(--roundness-bottom-right, var(--roundness, 100%));
				transform: var(--knob-offset, translate(-50%, -50%));
				background: var(--knob-color, var(--slider-color, white));
				border: var(--knob-border, none);
				box-shadow: var(--knob-box-shadow, none);
			}
			.knob:focus {
				background-color: red;
			}
			.value {
				opacity: var(--value-opacity, 0.5);
				pointer-events: none;
				text-align: var(--value-align, center);
				position: relative;
				top: var(--value-offset, 100%);
				font: inherit;
				color: inherit;
				margin: var(--value-margin, 0 -100px);
				-webkit-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}
		`;

		// attach the created elements to the shadow dom

		shadow.appendChild(this.css);
		shadow.appendChild(this.knob = this.renderKnob());
	}

	attributeChangedCallback(name: string, oldValue: string, newValue: string) {
		switch (name) {
			case "value":
				this.valueElem.textContent = newValue;
				break;
			case "min":
			case "max":
			case "slope":
				break;
			default:
				console.warn("unknown property change", name, oldValue, newValue);
				break;
		}
	}

	renderKnob(): HTMLElement {
		let knob = document.createElement("div");
		knob.className = "knob";
		knob.appendChild(this.valueElem = this.renderValue());
		return knob;
	}

	renderValue(): HTMLElement {
		let value = document.createElement("div");
		value.className = "value";
		value.textContent = this.value;
		return value;
	}

	static get observedAttributes() { return ["axis", "value"]; }

	get axis(): KnobAxis {
		let attr = this.getAttribute("axis");
		if (attr == "x" || attr == "y" || attr == "both")
			return attr;
		else
			return "x";
	}
	set axis(value: KnobAxis) {
		if (value == "x" || value == "y" || value == "both")
			this.setAttribute("axis", value);
	}

	get valueNum(): number { return this._valueNum; }
	set valueNum(value: number) { this._valueNum = value; }

	get value(): string { return this.getAttribute("value") || "0"; }
	set value(value: string) { this.setAttribute("value", value); }

	get moveX(): boolean {
		return this.axis == "x" || this.axis == "both";
	}

	get moveY(): boolean {
		return this.axis == "y" || this.axis == "both";
	}
}
window.customElements.define("slider-knob", SliderKnob);
