import { CurveSlider, SliderKnob } from "./CurveSlider";

export class RangeCurveSlider extends CurveSlider {
	knobs: [SliderKnob, SliderKnob];

	constructor() {
		// Always call super first in constructor
		super();

		this.css.textContent += `
			slider-knob.min {
				--knob-width: 12px;
				--knob-height: 16px;
				--roundness-top-left: 16px;
				--roundness-bottom-left: 16px;
				--roundness-top-right: 4px;
				--roundness-bottom-right: 4px;
				--knob-offset: translate(-100%, -50%);
				--value-align: right;
				--value-margin: 0 2px 0 -100px;

				left: calc(var(--min-normalized-value) * 100%);
			}

			slider-knob.max {
				--knob-width: 12px;
				--knob-height: 16px;
				--roundness-top-left: 4px;
				--roundness-bottom-left: 4px;
				--roundness-top-right: 16px;
				--roundness-bottom-right: 16px;
				--knob-offset: translate(0, -50%);
				--value-align: left;
				--value-margin: 0 -100px 0 2px;

				left: calc(var(--max-normalized-value) * 100%);
			}
		`;
	}

	render(shadow: ShadowRoot): void {
		this.container = document.createElement("div");
		this.container.addEventListener("pointerdown", e => {
			e.preventDefault();
			this.handleBarDrag(this.container, e);
		});
		this.container.className = "container";
		this.container.appendChild(this.bar = this.renderBar());
		this.knobs = this.renderKnobs();
		this.container.appendChild(this.knobs[0]);
		this.container.appendChild(this.knobs[1]);
		this.regenerateTicks();
		shadow.appendChild(this.container);
	}

	attributeChangedCallback(name: string, oldValue: string, newValue: string) {
		switch (name) {
			case "min-value":
				this.knobs[0].valueNum = parseFloat(newValue);
				this.knobs[0].value = this.formatValue(parseFloat(newValue));
				this.recompute();
				break;
			case "max-value":
				this.knobs[1].valueNum = parseFloat(newValue);
				this.knobs[1].value = this.formatValue(parseFloat(newValue));
				this.recompute();
				break;
			case "min":
			case "max":
			case "slope":
				this.recompute();
			/* falls through */
			case "tick-count":
				this.regenerateTicks();
				break;
			case "max-infinity":
			case "min-infinity":
				this.knobs[0].value = this.formatValue(this.knobs[0].valueNum);
				this.knobs[1].value = this.formatValue(this.knobs[1].valueNum);
				break;
			default:
				console.warn("unknown property change", name, oldValue, newValue);
				break;
		}
	}

	renderKnobs(): [SliderKnob, SliderKnob] {
		let ret: [SliderKnob, SliderKnob] = [
			this.renderKnob(this.minValue, 1),
			this.renderKnob(this.maxValue, 2)
		];
		ret[0].classList.add("min");
		ret[1].classList.add("max");
		return ret;
	}

	handleKnobDrag(knob: SliderKnob, e: PointerEvent): void {
		const rect = this.getBoundingClientRect();
		const min = this.min;
		const max = this.max;
		let v = (e.clientX - rect.left) / rect.width;
		if (v < 0) v = 0;
		if (v > 1) v = 1;
		let t = "";
		if (knob.classList.contains("min")) {
			const localMax = this.untransform(this.maxValue);
			if (v > localMax)
				v = localMax;
			t = "min";
		}
		else if (knob.classList.contains("max")) {
			const localMin = this.untransform(this.minValue);
			if (v < localMin)
				v = localMin;
			t = "max";
		}
		else console.error("don't know what i'm dragging!!");
		let vt = this.snapClamp(this.transform(v));
		v = this.untransform(vt);
		knob.valueNum = vt;
		knob.value = this.formatValue(vt);

		this.style.setProperty("--" + t + "-input-value", vt.toString());
		this.style.setProperty("--" + t + "-normalized-value", v.toString());

		this.updateTicks();
	}

	handleBarDrag(bar: HTMLElement, e: PointerEvent): void {
		const rect = this.getBoundingClientRect();
		let v = (e.clientX - rect.left) / rect.width;
		let mid = (this.untransform(this.minValue) + this.untransform(this.maxValue)) * 0.5;
		const knob = v < mid ? this.knobs[0] : this.knobs[1];
		this.startKnobDrag(knob, e.pointerId);
	}

	finishKnobDrag(knob: SliderKnob, value: number): void {
		if (value === undefined || value === null)
			return;

		if (knob.classList.contains("min")) {
			this.minValue = value;
		}
		else if (knob.classList.contains("max")) {
			this.maxValue = value;
		}
		else console.error("don't know what i'm dragging!!");
	}

	nudgeKnob(knob: SliderKnob, value: number): void {
		if (knob.classList.contains("min")) {
			const t = this.minValue + this.calculateNudge(this.minValue, value);
			if (t < this.maxValue)
				this.minValue = t;
			else
				this.minValue = this.maxValue;
		}
		else if (knob.classList.contains("max")) {
			const t = this.maxValue + this.calculateNudge(this.maxValue, value);
			if (t > this.minValue)
				this.maxValue = t;
			else
				this.maxValue = this.minValue;
		}
		else console.error("don't know what kind of knob this is!!");
	}

	static get observedAttributes() { return ["min", "max", "slope", "min-value", "max-value", "tick-count", "max-infinity", "min-infinity"]; }

	recompute(): void {
		if (this.minValue > this.max)
			this.minValue = this.max;
		else if (this.minValue < this.min)
			this.minValue = this.min;

		if (this.maxValue > this.max)
			this.maxValue = this.max;
		else if (this.maxValue < this.min)
			this.maxValue = this.min;

		if (this.minValue > this.maxValue)
			this.minValue = this.maxValue;


		this.style.setProperty("--min-input-value", this.minValue.toString());
		this.style.setProperty("--min-normalized-value", this.untransform(this.minValue).toString());

		this.style.setProperty("--max-input-value", this.maxValue.toString());
		this.style.setProperty("--max-normalized-value", this.untransform(this.maxValue).toString());

		this.updateTicks();
	}

	/**
	 * checks if a given normalized value is in the user selected value range of this slider.
	 * @param v a normalized value in range 0-1
	 */
	inSlider(v: number): boolean {
		return v >= parseFloat(this.style.getPropertyValue("--min-normalized-value"))
			&& v <= parseFloat(this.style.getPropertyValue("--max-normalized-value"));
	}

	formatValue(v: number): string {
		if (v + 0.000001 >= this.max && this.maxInfinity)
			return "∞";
		else if (v - 0.000001 <= this.min && this.minInfinity)
			return "-∞";

		const a = Math.abs(v);
		if (a > 1000)
			return Math.round(v).toString();
		else if (a > 10)
			return (Math.round(v * 10) / 10).toString();
		else
			return (Math.round(v * 100) / 100).toString();
	}

	get minValue(): number { return parseFloat(this.getAttribute("min-value") || "0"); }
	set minValue(minValue: number) { this.setAttribute("min-value", minValue.toString()); }

	get maxValue(): number { return parseFloat(this.getAttribute("max-value") || "1"); }
	set maxValue(maxValue: number) { this.setAttribute("max-value", maxValue.toString()); }
}
window.customElements.define('range-curve-slider', RangeCurveSlider);
