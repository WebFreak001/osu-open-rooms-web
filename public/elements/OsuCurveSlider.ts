import { CurveSlider } from "./CurveSlider";
import { RangeCurveSlider } from "./RangeCurveSlider";

export class OsuCurveSlider extends CurveSlider {
	constructor() {
		// Always call super first in constructor
		super();

		this.css.textContent += `
			:host {
				--bar-height: 3px;
				--slider-color: #ff66aa;
				--knob-width: 30px;
				--gradstart: calc(var(--normalized-value) * 100% - var(--knob-width) / 2);
				--gradend: calc(var(--normalized-value) * 100% + var(--knob-width) / 2);
				--bar-color: linear-gradient(90deg,
					#ff66aa var(--gradstart),
					transparent var(--gradstart),
					transparent var(--gradend),
					rgba(255, 102, 170, 0.6) var(--gradend));
				--tick-color: #3e3a44;
				--tick-opacity: 1;
				--tick-size: 3px;
			}

			.tick.between {
				--tick-opacity: 0;
			}

			:focus-within .bar {
				box-shadow: none;
			}

			slider-knob {
				--knob-height: 9px;
				--roundness: 9px;
				--knob-border-color: #ff66aa;
				--knob-border: 3px solid var(--knob-border-color);
				--knob-color: transparent;
			}

			:hover slider-knob {
				--knob-color: #bf187f;
			}

			slider-knob:focus {
				--knob-box-shadow: 0 0 5px 3px #bf187f;
				--knob-color: var(--knob-border-color);
			}

			:hover slider-knob {
				--knob-box-shadow: 0 0 5px 3px #bf187f;
				--knob-border-color: #ffddee;
			}
		`;
	}
}
window.customElements.define("osu-curve-slider", OsuCurveSlider);


export class OsuRangeCurveSlider extends RangeCurveSlider {
	constructor() {
		// Always call super first in constructor
		super();

		this.css.textContent += `
			:host {
				--bar-height: 3px;
				--slider-color: #ff66aa;
				--gradminstart: calc(var(--min-normalized-value) * 100% - 20px);
				--gradminend: calc(var(--min-normalized-value) * 100%);
				--gradmaxstart: calc(var(--max-normalized-value) * 100%);
				--gradmaxend: calc(var(--max-normalized-value) * 100% + 20px);
				--bar-color: linear-gradient(90deg,
					rgba(255, 102, 170, 0.6) var(--gradminstart),
					transparent var(--gradminstart),
					transparent var(--gradminend),
					#ff66aa var(--gradminend),
					#ff66aa var(--gradmaxstart),
					transparent var(--gradmaxstart),
					transparent var(--gradmaxend),
					rgba(255, 102, 170, 0.6) var(--gradmaxend))
				;
				--tick-color: #3e3a44;
				--tick-opacity: 1;
				--tick-size: 3px;
			}

			.tick.between {
				--tick-opacity: 0;
			}

			:focus-within .bar {
				box-shadow: none;
			}

			slider-knob.min, slider-knob.max {
				--knob-width: 15px;
				--knob-height: 9px;
				--roundness: 9px;
				--knob-border-color: #ff66aa;
				--knob-border: 3px solid var(--knob-border-color);
				--knob-color: transparent;
			}

			:hover slider-knob {
				--knob-color: #bf187f;
			}

			slider-knob:focus {
				--knob-box-shadow: 0 0 5px 3px #bf187f;
				--knob-color: var(--knob-border-color);
			}

			:hover slider-knob {
				--knob-box-shadow: 0 0 5px 3px #bf187f;
				--knob-border-color: #ffddee;
			}
		`;
	}
}
window.customElements.define("osu-range-curve-slider", OsuRangeCurveSlider);
