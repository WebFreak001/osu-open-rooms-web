//@ts-check

/**
 * @param {HTMLAnchorElement} a
 */
function handleLike(a) {
	var like = a.getAttribute("data-like") == "true";
	var id = a.getAttribute("data-maplist");
	var count = a.previousElementSibling;
	var oldContent = count.textContent.trim();
	var icon = a.children[0];

	count.textContent = (parseInt(oldContent) + (like ? -1 : 1)).toString();
	icon.classList.toggle("fas");
	icon.classList.toggle("far");

	var xhr = new XMLHttpRequest();
	xhr.onloadend = function () {
		if (xhr.status >= 400) {
			icon.classList.toggle("fas");
			icon.classList.toggle("far");

			count.textContent = oldContent;
		}
		else {
			var likeCheck = new XMLHttpRequest();
			likeCheck.open("GET", "/api/maplists/" + id + "/likes");
			likeCheck.onload = function () {
				count.textContent = parseInt(likeCheck.responseText).toString();
			};
			likeCheck.send();
		}
	};

	xhr.open("GET", a.href);
	xhr.setRequestHeader("X-Refresh-Token", "false");
	xhr.send();

	if (a.href.indexOf("/unlike") != -1)
		a.href = a.href.replace("/unlike", "/like");
	else
		a.href = a.href.replace("/like", "/unlike");

	a.setAttribute("data-like", like ? "false" : "true");
}