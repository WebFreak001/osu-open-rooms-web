function loadTabs() {
	var tabcontrol = document.querySelector(".admin .tabcontrol");
	var tabs = document.querySelector(".admin .tabs");
	tabs.classList.add("js");
	tabcontrol.style.display = "block";

	var links = tabcontrol.querySelectorAll("a");
	var activeTab = links[0];
	activeTab.classList.add("active");
	tabs.getElementsByClassName(activeTab.getAttribute("data-tab"))[0].style.display = "block";

	for (var i = 0; i < links.length; i++) {
		var link = links[i];
		link.onclick = function () {
			tabs.getElementsByClassName(activeTab.getAttribute("data-tab"))[0].style.display = "";
			activeTab.classList.remove("active");

			activeTab = this;
			activeTab.classList.add("active");
			tabs.getElementsByClassName(activeTab.getAttribute("data-tab"))[0].style.display = "block";
		};
	}
}

function connectAdmin() {
	var chatMessages = document.querySelector(".chat .messages");
	var mapPreview = document.querySelector(".admin .info span.map");
	chatMessages.scrollTop = chatMessages.scrollHeight;
	if (window.WebSocket) {
		var url = window.location.origin + "/lobby/" + lobbyId + "/chat";

		if (url.startsWith("https:") || url.startsWith("http:"))
			url = "ws" + url.substr(4);

		var empty = chatMessages.querySelector(".empty");

		function onChatMessage(msg) {
			var wasDown = chatMessages.scrollTop > chatMessages.scrollHeight - chatMessages.clientHeight - 10;

			var elem = document.createElement("div");
			elem.className = "message" + (botAccounts.indexOf(msg.sender) == -1 ? "" : " bot");
			var time = document.createElement("div");
			time.className = "time utc";
			time.setAttribute("data-time", msg.time);
			time.textContent = new Date(msg.time).toLocaleTimeString();
			var sender = document.createElement("div");
			sender.className = "sender";
			sender.textContent = msg.sender;
			var content = document.createElement("div");
			content.className = "content";
			content.textContent = msg.message;
			elem.appendChild(time);
			elem.appendChild(sender);
			elem.appendChild(content);
			chatMessages.appendChild(elem);

			if (wasDown)
				chatMessages.scrollTop = chatMessages.scrollHeight;
		}

		function onMapChange(map) {
			if (map) {
				while (mapPreview.lastChild)
					mapPreview.removeChild(mapPreview.lastChild);

				var a = document.createElement("a");
				a.href = "https://osu.ppy.sh/b/" + map.beatmapID;
				a.textContent = map.artist + " - " + map.title + " [" + map.difficultyName + "]";
				mapPreview.appendChild(a);
			}
		}

		function connectChat(url) {
			var chatSocket = new WebSocket(url);

			chatSocket.onmessage = function (packet) {
				empty.style.display = "none";

				var msg = packet.data;
				if (typeof msg == "string")
					msg = JSON.parse(msg);

				console.log(msg);

				switch (msg.type) {
					case "chat":
						onChatMessage(msg.data);
						break;
					case "map":
						onMapChange(msg.data);
						break;
				}
			};

			chatSocket.onclose = function () {
				setTimeout(function () {
					console.error("Reconnecting websocket");

					connectChat(url);
				}, 1000);
			};
		}

		connectChat(url);
	}
}

window.loadTabs = loadTabs;
window.connectAdmin = connectAdmin;
