import "./elements/CurveSlider";
import "./elements/RangeCurveSlider";
import "./elements/OsuCurveSlider";

import "./js/locale"
import "./js/lobby_admin"
import "./js/mapselect"
import "./js/maplists_view"
import "./js/maplists"
import "./js/queue"
